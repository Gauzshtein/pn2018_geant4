//
/// \file RunAction.cc
/// \brief Implementation of the RunAction class

#include "RunAction.hh"
#include "g4root.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction()
 : G4UserRunAction()
{ 
  // set printing event number per each 100 events
 // G4RunManager::GetRunManager()->SetPrintProgress(1000);     
    
  auto analysisManager = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManager->GetType() << G4endl;
  
  // Create directories 
  //analysisManager->SetHistoDirectoryName("histograms");
  //analysisManager->SetNtupleDirectoryName("ntuple");
  analysisManager->SetVerboseLevel(1);
  //analysisManager->SetNtupleMerging(true);
  analysisManager->SetFirstNtupleId(1);

  // Creating ntuple
  //
  analysisManager->CreateNtuple("PN2018", "gd->XXX");
  
  analysisManager->CreateNtupleDColumn("eg");
  analysisManager->CreateNtupleDColumn("nreac");
  analysisManager->CreateNtupleDColumn("np");
  
  analysisManager->CreateNtupleDColumn("idp1");
  analysisManager->CreateNtupleDColumn("e1");
  analysisManager->CreateNtupleDColumn("tet1");
  analysisManager->CreateNtupleDColumn("fi1");
  analysisManager->CreateNtupleDColumn("idp2");
  analysisManager->CreateNtupleDColumn("e2");
  analysisManager->CreateNtupleDColumn("tet2");
  analysisManager->CreateNtupleDColumn("fi2");
  analysisManager->CreateNtupleDColumn("m12");
  
  analysisManager->CreateNtupleDColumn("depp1"); //write de for PP
  analysisManager->CreateNtupleDColumn("depp2");
  analysisManager->CreateNtupleDColumn("depp3");
  analysisManager->CreateNtupleDColumn("depp4");
  analysisManager->CreateNtupleDColumn("depp5");
  analysisManager->CreateNtupleDColumn("depp6");
  
  analysisManager->CreateNtupleDColumn("tpp1"); //write time for PP
  analysisManager->CreateNtupleDColumn("tpp2");
  analysisManager->CreateNtupleDColumn("tpp3");
  analysisManager->CreateNtupleDColumn("tpp4");
  analysisManager->CreateNtupleDColumn("tpp5");
  analysisManager->CreateNtupleDColumn("tpp6");
  
  analysisManager->CreateNtupleDColumn("deac1"); //write de for AC
  analysisManager->CreateNtupleDColumn("deac2");
  analysisManager->CreateNtupleDColumn("deac3");
  analysisManager->CreateNtupleDColumn("deac4");
  
  analysisManager->CreateNtupleDColumn("tac1"); //write time for AC
  analysisManager->CreateNtupleDColumn("tac2");
  analysisManager->CreateNtupleDColumn("tac3");
  analysisManager->CreateNtupleDColumn("tac4");
 
  //G4cout<<"!!!"<<"!!!"<<G4endl; 
  analysisManager->CreateNtupleDColumn("destrip1"); //write de for strip plastic
  analysisManager->CreateNtupleDColumn("destrip2");
  analysisManager->CreateNtupleDColumn("destrip3");
  analysisManager->CreateNtupleDColumn("destrip4");
  analysisManager->CreateNtupleDColumn("destrip5");
  analysisManager->CreateNtupleDColumn("destrip6");
  analysisManager->CreateNtupleDColumn("destrip7");
  analysisManager->CreateNtupleDColumn("destrip8");
  analysisManager->CreateNtupleDColumn("destrip9");
  analysisManager->CreateNtupleDColumn("destrip10");
  analysisManager->CreateNtupleDColumn("destrip11"); 
  analysisManager->CreateNtupleDColumn("destrip12");
  analysisManager->CreateNtupleDColumn("destrip13");
  analysisManager->CreateNtupleDColumn("destrip14");
  analysisManager->CreateNtupleDColumn("destrip15");
  analysisManager->CreateNtupleDColumn("destrip16");
  analysisManager->CreateNtupleDColumn("destrip17");
  analysisManager->CreateNtupleDColumn("destrip18");
  analysisManager->CreateNtupleDColumn("destrip19");
  analysisManager->CreateNtupleDColumn("destrip20");
  analysisManager->CreateNtupleDColumn("destrip21"); //write de for strip plastic
  analysisManager->CreateNtupleDColumn("destrip22");
  analysisManager->CreateNtupleDColumn("destrip23");
  analysisManager->CreateNtupleDColumn("destrip24");
  analysisManager->CreateNtupleDColumn("destrip25");
  analysisManager->CreateNtupleDColumn("destrip26");
  analysisManager->CreateNtupleDColumn("destrip27");
  analysisManager->CreateNtupleDColumn("destrip28");
  analysisManager->CreateNtupleDColumn("destrip29");
  analysisManager->CreateNtupleDColumn("destrip30");
  analysisManager->CreateNtupleDColumn("destrip31"); //write de for strip plastic
  analysisManager->CreateNtupleDColumn("destrip32");
  analysisManager->CreateNtupleDColumn("destrip33");
  analysisManager->CreateNtupleDColumn("destrip34");
  analysisManager->CreateNtupleDColumn("destrip35");
  analysisManager->CreateNtupleDColumn("destrip36");
  analysisManager->CreateNtupleDColumn("destrip37");
  analysisManager->CreateNtupleDColumn("destrip38");
  analysisManager->CreateNtupleDColumn("destrip39");
  analysisManager->CreateNtupleDColumn("destrip40");
  analysisManager->CreateNtupleDColumn("destrip41"); //write de for strip plastic
  analysisManager->CreateNtupleDColumn("destrip42");
  analysisManager->CreateNtupleDColumn("destrip43");
  analysisManager->CreateNtupleDColumn("destrip44");
  
  analysisManager->CreateNtupleDColumn("dee1"); //write de for electron strip plactic
  analysisManager->CreateNtupleDColumn("dee2");
  analysisManager->CreateNtupleDColumn("dee3");
  analysisManager->CreateNtupleDColumn("dee4");
  analysisManager->CreateNtupleDColumn("dee5");
  analysisManager->CreateNtupleDColumn("dee6");
  analysisManager->CreateNtupleDColumn("dee7");
  analysisManager->CreateNtupleDColumn("dee8");
  analysisManager->CreateNtupleDColumn("dee9");
  
  analysisManager->CreateNtupleDColumn("dee"); //write de for electron polystirol
  
  analysisManager->FinishNtuple();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{ delete G4AnalysisManager::Instance(); }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run*)
{ 
  //inform the runManager to save random number seed
//  G4RunManager::GetRunManager()->SetRandomNumberStore(false);
    
    
  // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  // Open an output file
  //
  G4String fileName = "pn2018_eg_390_650";
  analysisManager->OpenFile(fileName);
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run* )
{
    
   auto analysisManager = G4AnalysisManager::Instance();
//  if ( analysisManager->GetH1(0) ) {
//    G4cout << G4endl << " ----> print histograms statistic ";
//    if(isMaster) { G4cout << "for the entire run " << G4endl << G4endl;   }
//            else { G4cout << "for the local thread " << G4endl << G4endl; }
    
    //G4cout << " depp1 : mean = " 
    //   << G4BestUnit(analysisManager->GetH1(0)->mean(), "Energy") 
    //   << " rms = " 
    //   << G4BestUnit(analysisManager->GetH1(0)->rms(),  "Energy") << G4endl;
//  }
  
  analysisManager->Write();
  analysisManager->CloseFile();
  
}



