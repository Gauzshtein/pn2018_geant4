//
/// \file ElectronPolystirolHit.cc
/// \brief Implementation of the ElectronPolystirolHit class

#include "ElectronPolystirolHit.hh"

#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4AttDefStore.hh"
#include "G4AttDef.hh"
#include "G4AttValue.hh"
#include "G4UIcommand.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"


G4ThreadLocal G4Allocator<ElectronPolystirolHit>* ElectronPolystirolHitAllocator=0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
ElectronPolystirolHit::ElectronPolystirolHit(G4int id,G4double time, G4double de)
: G4VHit(), 
  fId(id), fTime(time), fde(de)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ElectronPolystirolHit::~ElectronPolystirolHit()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ElectronPolystirolHit::ElectronPolystirolHit(const ElectronPolystirolHit &right)
: G4VHit(),
  fId(right.fId),
  fTime(right.fTime),
  fde(right.fde)
 {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const ElectronPolystirolHit& ElectronPolystirolHit::operator=(const ElectronPolystirolHit &right)
{
  fId = right.fId;
  fTime = right.fTime;
  fde = right.fde;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int ElectronPolystirolHit::operator==(const ElectronPolystirolHit &/*right*/) const
{
  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
