//*********************************************//
//**** THIS CLASS FOR WORK WITH 4-VECTORS ****//
//*******************************************//


#include <iostream>
#include <math.h>
//using namespace std;
class p4vector {
private:
  double ve, vx, vy, vz, vtet, vfi, vmm;
 public:
  p4vector(double se=.0, double sx=.0, double sy=.0, double sz=.0, double stet=.0, double sfi=.0, double smm=.0) : ve(se), vx(sx), vy(sy), vz(sz), vtet(stet), vfi(sfi), vmm(smm) {};
  double& e() { return ve; }
  double& mm() { return vmm; }
  double& x() { return vx; }
  double& y() { return vy; }
  double& z() { return vz; }
  double& tet() { return vtet; }
  double& fi() { return vfi; }
  double p() { return sqrt(vx*vx+vy*vy+vz*vz); }
  double mmr() { return ve*ve-vx*vx-vy*vy-vz*vz; }
  double tetr() { return acos(vz/(sqrt(vx*vx+vy*vy+vz*vz))); }
  double fir()
	      {
	      return atan2(vy, vx);
/*		if (vy/p()/sin(vtet)>.0)
		    {
		    if (vx/p()/sin(vtet)>.0) return asin(vy/p()/sin(vtet));
		    if (vx/p()/sin(vtet)<.0) return acos(vx/p()/sin(vtet));
		    }
		if (vy/p()/sin(vtet)<.0)
		    {
		    if (vx/p()/sin(vtet)>.0) return asin(vy/p()/sin(vtet));
		    if (vx/p()/sin(vtet)<.0) return -1.*acos(vx/p()/sin(vtet));
		    }*/
		}
	p4vector operator + (const p4vector a) { return p4vector(ve+a.ve, vx+a.vx, vy+a.vy, vz+a.vz); }
	p4vector operator - (const p4vector a) { return p4vector(ve-a.ve, vx-a.vx, vy-a.vy, vz-a.vz); }
	

  };
  
//int main()
//{
//p4vec p1;
//p1.x()=p1.y()=p1.z()=1.;
//p1.tet()=p1.tetr();
//}
