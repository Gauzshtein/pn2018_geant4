//
/// \file StripPlasticSD.cc
/// \brief Implementation of the StripPlasticSD class

#include "StripPlasticSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

StripPlasticSD::StripPlasticSD(G4String name) : 
G4VSensitiveDetector(name),
fHitsCollection(nullptr), fHCID(-1)
                      { collectionName.insert("StripPlasticCollection"); }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

StripPlasticSD::~StripPlasticSD() 
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void StripPlasticSD::Initialize(G4HCofThisEvent* hce)
{
  // Create hits collection for Strip plastic
  fHitsCollection = new StripPlasticHitsCollection(SensitiveDetectorName, collectionName[0]); 
  // Add this collection in hce
  if (fHCID<0) { 
    fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(fHitsCollection); 
  }
  hce->AddHitsCollection(fHCID,fHitsCollection);
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool StripPlasticSD::ProcessHits(G4Step* aStep, 
                                     G4TouchableHistory*)
{  
  G4double edep = aStep->GetTotalEnergyDeposit(); //Get energy deposit
  if (edep==0.) return true;
  auto preStepPoint = aStep->GetPreStepPoint();
  G4int copyNoStrip = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(0); //number of Strip
  G4int copyNoFe = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(1); //number of Fe
  G4int copyNoLayer = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(2); //number of Layer
  //G4cout<<G4endl<<copyNoStrip<<"\t"<<copyNoFe<<"\t"<<copyNoLayer<<G4endl;
  G4int copyNo = copyNoStrip + 2*copyNoFe + copyNoLayer;
  //G4cout<<G4endl<<copyNo<<G4endl;
  auto hitTime = preStepPoint->GetGlobalTime(); //Get time for Strip plastic

  // check if this finger already has a hit
  G4int ix = -1;
  for (G4int i=0;i<fHitsCollection->entries();i++) {
    if ((*fHitsCollection)[i]->GetID()==copyNo) {
      ix = i;
      break;
    }
  }

  if (ix>=0) {
    if ((*fHitsCollection)[ix]->GetTime()>hitTime) { (*fHitsCollection)[ix]->SetTime(hitTime);}
    (*fHitsCollection)[ix]->AddEnergy(edep); }
  else {
    // if not, create a new hit and set it to the collection
    StripPlasticHit* hit = new StripPlasticHit(copyNo,hitTime,edep);
    fHitsCollection->insert(hit);
  }
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
void B1PlasticSD::EndOfEvent(G4HCofThisEvent*)
{
  if ( verboseLevel>1 ) { 
     G4int nofHits = fHitsCollection->entries();
     G4cout << G4endl
            << "-------->Hits Collection: in this event they are " << nofHits 
            << " hits in the tracker chambers: " << G4endl;
     for ( G4int i=0; i<nofHits; i++ ) (*fHitsCollection)[i]->Print();
  }
}
*/
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
