//
/// \file ElectronStripPlasticHit.cc
/// \brief Implementation of the ElectronStripPlasticHit class

#include "ElectronStripPlasticHit.hh"

#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4AttDefStore.hh"
#include "G4AttDef.hh"
#include "G4AttValue.hh"
#include "G4UIcommand.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"


G4ThreadLocal G4Allocator<ElectronStripPlasticHit>* ElectronStripPlasticHitAllocator=0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
ElectronStripPlasticHit::ElectronStripPlasticHit(G4int id,G4double time, G4double de)
: G4VHit(), 
  fId(id), fTime(time), fde(de)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ElectronStripPlasticHit::~ElectronStripPlasticHit()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ElectronStripPlasticHit::ElectronStripPlasticHit(const ElectronStripPlasticHit &right)
: G4VHit(),
  fId(right.fId),
  fTime(right.fTime),
  fde(right.fde)
 {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const ElectronStripPlasticHit& ElectronStripPlasticHit::operator=(const ElectronStripPlasticHit &right)
{
  fId = right.fId;
  fTime = right.fTime;
  fde = right.fde;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int ElectronStripPlasticHit::operator==(const ElectronStripPlasticHit &/*right*/) const
{
  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
