# include "ConfigPN.hh"

ConfigPN* ConfigPN::_self = nullptr;

ConfigPN::ConfigPN() :
        _rootFileName ("")
{
   
}

ConfigPN::~ConfigPN()
{
}

ConfigPN* ConfigPN::Instance()
{
    if ( ! _self )
    {
        _self = new ConfigPN();
    }
    return _self;
}
