//
/// \file ElectronStripPlasticSD.cc
/// \brief Implementation of the ElectronStripPlasticSD class

#include "ElectronStripPlasticSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ElectronStripPlasticSD::ElectronStripPlasticSD(G4String name) : 
G4VSensitiveDetector(name),
fHitsCollection(nullptr), fHCID(-1)
                      { collectionName.insert("ElectronStripPlasticCollection"); }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ElectronStripPlasticSD::~ElectronStripPlasticSD() 
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ElectronStripPlasticSD::Initialize(G4HCofThisEvent* hce)
{
  // Create hits collection 
  fHitsCollection = new ElectronStripPlasticHitsCollection(SensitiveDetectorName, collectionName[0]); 
  // Add this collection in hce
  if (fHCID<0) { 
    fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(fHitsCollection); 
  }
  hce->AddHitsCollection(fHCID,fHitsCollection);
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool ElectronStripPlasticSD::ProcessHits(G4Step* aStep, 
                                     G4TouchableHistory*)
{  
  G4double edep = aStep->GetTotalEnergyDeposit(); //Get energy deposit
  if (edep==0.) return true;
  auto preStepPoint = aStep->GetPreStepPoint();
  G4int copyNo = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(0); //Get number of electron strip plastic
//  if (copyNo==1) G4cout<<G4endl<<" AC number is "<<copyNo<<G4endl;
  auto hitTime = preStepPoint->GetGlobalTime(); //Get time 

  // check if this finger already has a hit
  G4int ix = -1;
  for (G4int i=0;i<fHitsCollection->entries();i++) {
    if ((*fHitsCollection)[i]->GetID()==copyNo) {
      ix = i;
      break;
    }
  }

  if (ix>=0) {
    if ((*fHitsCollection)[ix]->GetTime()>hitTime) { (*fHitsCollection)[ix]->SetTime(hitTime);}
    (*fHitsCollection)[ix]->AddEnergy(edep); }
  else {
    // if not, create a new hit and set it to the collection
    ElectronStripPlasticHit* hit = new ElectronStripPlasticHit(copyNo,hitTime,edep);
    fHitsCollection->insert(hit);
  }
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
void B1PlasticSD::EndOfEvent(G4HCofThisEvent*)
{
  if ( verboseLevel>1 ) { 
     G4int nofHits = fHitsCollection->entries();
     G4cout << G4endl
            << "-------->Hits Collection: in this event they are " << nofHits 
            << " hits in the tracker chambers: " << G4endl;
     for ( G4int i=0; i<nofHits; i++ ) (*fHitsCollection)[i]->Print();
  }
}
*/
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
