/// \file PrimaryGeneratorAction.cc
/// \brief Implementation of the PrimaryGeneratorAction class

#include "PrimaryGeneratorAction.hh"
#include "g4root.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "ConfigPN.hh"
#include "TTree.h"
#include "p4vector.cc"
//#include "povorot.cc"
//#include "dalits.cc"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0),
  fEnvelopeBox(0),
  evfile(NULL),
  T(NULL),
  i_entry(0),
  nentries(0),
  efot(-1),
  nreac(0),
  vx(0),
  vy(0),
  vz(0),
  np(0)
  {
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

//  evfile = new TFile(ConfigPN::Instance()->get_root_file_name().c_str()); //Bogdan
//  evfile->GetObject(ConfigPN::Instance()->get_tree_name().c_str(), T); //Bogdan
//  T->GetBranch("efot")->SetAddress(&efot); //Bogdan
  
  evfile = new TFile("/home/viacheslav/files_root/var_390_650.root");
  T = (TTree*)evfile->Get("h10");
  
  T->SetBranchAddress("efot", &efot);
  T->SetBranchAddress("nreac", &nreac);
  T->SetBranchAddress("vx", &vx);
  T->SetBranchAddress("vy", &vy);
  T->SetBranchAddress("vz", &vz);
  T->SetBranchAddress("np", &np);
  T->SetBranchAddress("idg", idg);
  T->SetBranchAddress("qch", qch);
  T->SetBranchAddress("mass", mass);
  T->SetBranchAddress("imp", imp);
  T->SetBranchAddress("cx", cx);
  T->SetBranchAddress("cy", cy);
  T->SetBranchAddress("cz", cz);
  
  nentries = (int)T->GetEntries();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
 // G4String particleName;
  G4ParticleDefinition* particle;
  particle = particleTable->FindParticle("gamma");
  G4double energy_particle;
  
  //T->GetBranch("efot")->GetEvent(i_entry); //Bogdan
  //i_entry++; //Bogdan
  
  T->GetEntry(i_entry++); 
  efot*=1000.*MeV;
     
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->FillNtupleDColumn(1,0, efot);
  analysisManager->FillNtupleDColumn(1,1, nreac);
  analysisManager->FillNtupleDColumn(1,2, np);
  //analysisManager->AddNtupleRow();  
  
  
  G4double ep=0.0, tetp=0.0, fip=0.0; // for proton
  G4double en=0.0, tetn=0.0, fin=0.0; // for neutron
  G4double ed=0.0, tetd=0.0, fid=0.0; // for deuteron
  G4int idp=0, idn=0;
  
  p4vector p1, p2, p12;
 // G4double m12;  
 
  for (G4int i=0; i<np; i++)          
  { //begin of reaction 
  energy_particle=(sqrt(imp[i]*imp[i]+mass[i]*mass[i])-mass[i])*GeV;
  //if (nreac==3) {  G4cout<<"idp"<<i<<"="<<idg[i]<<"\t"<<energy_particle<<"\t*******";  }
  //if (idg[i]==1) particle = particleTable->FindParticle("gamma");
  //if (idg[i]==2) particle = particleTable->FindParticle("e+"); 
  //if (idg[i]==3) particle = particleTable->FindParticle("e-");  //G4cout<<"e-\t";}
  //if (idg[i]==5) {particle = particleTable->FindParticle("mu+"); G4cout<<"mu+\t";}
  //if (idg[i]==6) {particle = particleTable->FindParticle("mu-"); G4cout<<"mu-\t";}
  if (idg[i]==7) {particle = particleTable->FindParticle("pi0");}// G4cout<<"pi0\t";}
  if (idg[i]==8) {particle = particleTable->FindParticle("pi+");}// G4cout<<"pi+\t";}
  if (idg[i]==9) {particle = particleTable->FindParticle("pi-");}// G4cout<<"pi-\t";}
  //if (idg[i]==10) {particle = particleTable->FindParticle("kaon0L"); G4cout<<"kaon0L\t";}
  //if (idg[i]==11) {particle = particleTable->FindParticle("kaon+"); G4cout<<"kaon+\t";}
  //if (idg[i]==12) {particle = particleTable->FindParticle("kaon-"); G4cout<<"kaon-\t";}
  if (idg[i]==13) 
    {
        en=energy_particle;
        //G4cout<<mass[i]<<G4endl;
        tetn=acos(cz[i]/sqrt(cx[i]*cx[i]+cy[i]*cy[i]+cz[i]*cz[i]));
        fin=atan2(cy[i], cx[i]);
        particle = particleTable->FindParticle("neutron");
        if (idn==0&&idp==0) //neuteron is first
                    {
                    analysisManager->FillNtupleDColumn(1, 3, 10); //id first nucleon
                    analysisManager->FillNtupleDColumn(1, 4, en);
                    analysisManager->FillNtupleDColumn(1, 5, tetn*180./3.14159);
                    analysisManager->FillNtupleDColumn(1, 6, fin*180./3.14159);
                    idn++;
                    p1.e()=en+mass[i]*1000.; p1.x()=imp[i]*cx[i]*1000.; p1.y()=imp[i]*cy[i]*1000.; p1.z()=imp[i]*cz[i]*1000.;
                  //  G4cout<<"p="<<imp[i]<<G4endl;
                    }
        if (idn==1||idp==1) //neutron is second
                    {
                    analysisManager->FillNtupleDColumn(1, 7, 10); //id second nucleon
                    analysisManager->FillNtupleDColumn(1, 8, en);
                    analysisManager->FillNtupleDColumn(1, 9, tetn*180./3.14159);
                    analysisManager->FillNtupleDColumn(1, 10, fin*180./3.14159);
                    p2.e()=en+mass[i]*1000.; p2.x()=imp[i]*cx[i]*1000.; p2.y()=imp[i]*cy[i]*1000.; p2.z()=imp[i]*cz[i]*1000.;
                    //idn++;
                    }            
    }
  
  if (idg[i]==14) 
   {  
        ep=energy_particle;
      //  G4cout<<"Ep="<<(sqrt(imp[i]*imp[i]+mass[i]*mass[i])-mass[i])<<"\t";
        tetp=acos(cz[i]/sqrt(cx[i]*cx[i]+cy[i]*cy[i]+cz[i]*cz[i]));
        fip=atan2(cy[i], cx[i]);
        particle = particleTable->FindParticle("proton");
        if (idn==0&&idp==0) //proton is first
                    {
                    analysisManager->FillNtupleDColumn(1, 3, 11); //id first nucleon
                    analysisManager->FillNtupleDColumn(1, 4, ep);
                    analysisManager->FillNtupleDColumn(1, 5, tetp*180./3.14159);
                    analysisManager->FillNtupleDColumn(1, 6, fip*180./3.14159);
                    idp++;
                    p1.e()=ep+mass[i]*1000.; p1.x()=imp[i]*cx[i]*1000.; p1.y()=imp[i]*cy[i]*1000.; p1.z()=imp[i]*cz[i]*1000.;
                    }
        if (idn==1||idp==1) //proton is second
                    {
                    analysisManager->FillNtupleDColumn(1, 7, 11); //id second nucleon
                    analysisManager->FillNtupleDColumn(1, 8, ep);
                    analysisManager->FillNtupleDColumn(1, 9, tetp*180./3.14159);
                    analysisManager->FillNtupleDColumn(1, 10, fip*180./3.14159);
                    //idn++;
                    p2.e()=ep+mass[i]*1000.; p2.x()=imp[i]*cx[i]*1000.; p2.y()=imp[i]*cy[i]*1000.; p2.z()=imp[i]*cz[i]*1000.;
                    }   
   }               
    
  //if (idg[i]==16) {particle = particleTable->FindParticle("kaon0S"); G4cout<<"kaon0S\t";}
  //if (idg[i]==17) particle = particleTable->FindParticle("eta"); //G4cout<<"eta\t";}
  //if (idg[i]==18) {particle = particleTable->FindParticle("lambda"); G4cout<<"lambda\t";}
  //if (idg[i]==19) {particle = particleTable->FindParticle("sigma+"); G4cout<<"sigma+\t";}
  //if (idg[i]==20) {particle = particleTable->FindParticle("sigma0"); G4cout<<"sigma0\t";}
  //if (idg[i]==21) {particle = particleTable->FindParticle("sigma-"); G4cout<<"sigma-\t";}
  if (idg[i]==45) //{particle = particleTable->FindParticle("deuteron"); G4cout<<"deuteron\t";}
     {
     ed=energy_particle;
     tetd=acos(cz[i]/sqrt(cx[i]*cx[i]+cy[i]*cy[i]+cz[i]*cz[i]));
     fid=atan2(cy[i], cx[i]);
     particle = particleTable->FindParticle("deuteron");
     analysisManager->FillNtupleDColumn(1, 3, 12); //id first nucleon
     analysisManager->FillNtupleDColumn(1, 4, ed);
     analysisManager->FillNtupleDColumn(1, 5, tetd*180./3.14159);
     analysisManager->FillNtupleDColumn(1, 6, fid*180./3.14159);
     }
  //if (idg[i]==57) {particle = particleTable->FindParticle("rho0"); G4cout<<"rho0\t";}
  //if (idg[i]==58) {particle = particleTable->FindParticle("rho+"); G4cout<<"rho+\t";}
  //if (idg[i]==59) {particle = particleTable->FindParticle("rho-"); G4cout<<"rho-\t";}
  //if (idg[i]==60) {particle = particleTable->FindParticle("omega"); G4cout<<"omega\t";}
    
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(cx[i],cy[i],cz[i])); 
  fParticleGun->SetParticleEnergy(energy_particle);
  fParticleGun->SetParticlePosition(G4ThreeVector(vx*cm,vy*cm,vz*cm));
  fParticleGun->GeneratePrimaryVertex(anEvent);
  
  }  //end of reaction
  p12=p1+p2;
  analysisManager->FillNtupleDColumn(1, 11, sqrt(p12.mmr()));
  //m12=sqrt(p12.mmr());
 // G4cout<<"m12="<<sqrt(p12.mmr())<<G4endl;
 // G4cout<<"p12.e="<<p12.e()<<"\t"<<"p12.z="<<p12.z()<<"\t"<<"p12.y="<<p12.y()<<"\t"<<"p12.x="<<p12.x()<<"\t";
 // G4cout<<"m12="<<sqrt(p12.e()*p12.e()-p12.z()*p12.z()-p12.y()*p12.y()-p12.x()*p12.x())<<G4endl;
  //G4cout<<"M12="<<sqrt(p12.mmr())<<G4endl; 

}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

