/// \file DetectorConstruction.cc
/// \brief Implementation of the DetectorConstruction class

#include "DetectorConstruction.hh"
#include "ProtonPlasticSD.hh"
#include "StripPlasticSD.hh"
#include "ACplasticSD.hh"
#include "ElectronStripPlasticSD.hh"
#include "ElectronPolystirolSD.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4SubtractionSolid.hh"
#include "G4Transform3D.hh"
#include "G4PVReplica.hh"
#include "G4SDManager.hh"
#include "G4UserLimits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Material.hh"
#include "G4SystemOfUnits.hh"
#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"


   
#define NX_BARS	22
#define NZ_BARS 22
#define N_UNITS	(NX_BARS/2)	/* assuming square calorimetr */
#define N_LAYERS 10
#define NVC_WRS 42
#define NW1_WRS	18
#define NW2_WRS	18
#define NW3_WRS	27
#define NLQ_BRS 7 
#define NCX_BRS 7 

#define AC_IND  0			/* 1 layer */
#define N_AC	1
#define TOF_IND (AC_IND+N_AC)		/* RPC ? */
#define N_TOF	1
#define HCX_IND (TOF_IND+N_TOF)		/* X bars */
#define N_HCX	NX_BARS*N_LAYERS
#define HCZ_IND	(HCX_IND+N_HCX)		/* Z bars */
#define N_HCZ	NZ_BARS*N_LAYERS
#define DE_IND	(HCZ_IND+N_HCZ)		/* dE in Sandwich : 2 layers */
#define N_DE	1
#define VC_IND   (DE_IND+N_DE)		/* Vertex Ch */
#define WC1_IND  (VC_IND+NVC_WRS) 	/* Drift Chambers */
#define WC2_IND  (WC1_IND+NW1_WRS)	/* Drift Chambers */
#define WC3_IND  (WC2_IND+NW2_WRS)	/* Drift Chambers */
#define LQ_IND   (WC3_IND+NW3_WRS)	/* future use */
//#define ECX_IND  (LQ_IND+NLQ_BRS)	/* X bars ?*/
//#define ECZ_IND  (ECX_IND+NCX_BRS)	/* Z bars ?*/
#define ARM1_IND 0
#define ARM2_IND LQ_IND

#define NSENS	ARM2_IND*2

// detector element type
#define DT_WIRE		0
#define DT_SCINT	1
#define DT_EMCAL	2


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
  {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{ 
    G4double world_sizeX =4.*meter, world_sizeY = 4.*meter, world_sizeZ =4.*meter; //Size of the World
    
    //DefineMaterials();
    //DefineRotationMatrices();
    G4NistManager* nist = G4NistManager::Instance();

    G4Material* material_AIR = nist->FindOrBuildMaterial("G4_AIR"); //material of the World
    
    G4Material* material_STEEL = nist->FindOrBuildMaterial("G4_STAINLESS-STEEL"); //material of the STEEL
    auto color_STEEL = new G4VisAttributes(G4Colour(0.0,0.0,1.0));   // color of STEEL
    
    G4Material* material_ALUMIN = nist->FindOrBuildMaterial("G4_Al"); //material of the ALUMIN
    auto color_ALUMIN = new G4VisAttributes(G4Colour(0.0,0.8,0.8));   // color of ALUMIN
        
    G4Material* material_TITAN = nist->FindOrBuildMaterial("G4_Ti"); //material of the TITAN
    auto color_TITAN = new G4VisAttributes(G4Colour(0.8,0.8,0.8));   // color of TITAN

    G4Material* material_SIO_2 = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE"); //material of the SIO_2
    auto color_SIO_2 = new G4VisAttributes(G4Colour(0.742, 0.699, 0.121));   // color of SIO_2

    G4Material* material_CO_2 = nist->FindOrBuildMaterial("G4_CARBON_DIOXIDE"); //material of the CO_2
    G4Material* material_AR = nist->FindOrBuildMaterial("G4_Ar"); //material of the Ar

    G4Material* material_ARCO_2 = new G4Material("ARCO_2", 1.98e-03*g/cm3, 2);
    material_ARCO_2->AddMaterial(material_AR, .2);
    material_ARCO_2->AddMaterial(material_CO_2, .8);
    auto color_ARCO_2 = new G4VisAttributes(G4Colour(0.93, 0.77, 0.57));   // color of ARCO_2
    
    G4Material* material_VACUUM = nist->FindOrBuildMaterial("G4_Galactic"); //material of the VACUUM

    G4Material* material_MYLAR = nist->FindOrBuildMaterial("G4_MYLAR"); //material of the MYLAR
    auto color_MYLAR = new G4VisAttributes(G4Colour(0.5, 0.7, 0.2));   // color of MYLAR    
    
    G4Material* material_SCINTIL = nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE"); //material of the Plastic Scintillator  
    auto color_SCINTIL = new G4VisAttributes(G4Colour(1.0, 0.0, 1.0));   // color of the Plastic Scintillator
    
    G4Material* material_Pb = nist->FindOrBuildMaterial("G4_Pb"); //material of the Pb  
    auto color_Pb = new G4VisAttributes(G4Colour(0.6, 0.6, 0.6));   // color of the Pb

    G4Material* material_PLEXIGLASS = nist->FindOrBuildMaterial("G4_PLEXIGLASS"); //material of the PLEXIGLASS  
    auto color_PLEXIGLASS = new G4VisAttributes(G4Colour(1.0, 0.6, 1.0));   // color of the PLEXIGLASS
    
    
     G4RotationMatrix Rotate180X; Rotate180X.rotateX(180*deg);		
     G4RotationMatrix Rotate180Y; Rotate180Y.rotateY(180*deg);
     G4RotationMatrix Rotate180Z; Rotate180Z.rotateZ(180*deg);	
     G4RotationMatrix Rotate270X; Rotate270X.rotateX(-90*deg);
     G4RotationMatrix Rotate90X; Rotate90X.rotateX(90*deg);	
     G4RotationMatrix Rotate9X;	Rotate9X.rotateX(9*deg);
     G4RotationMatrix Rotate99X; Rotate99X.rotateX(99*deg);	
     G4RotationMatrix Rotate45X; Rotate45X.rotateX(45*deg);		
     G4RotationMatrix Rotate45X180Z; Rotate45X180Z.rotateX(45*deg); Rotate45X180Z.rotateZ(180*deg);		
     G4RotationMatrix Rotate325X; Rotate325X.rotateX(-35*deg);		
     G4RotationMatrix Rotate10X; Rotate10X.rotateX(10*deg);	
     G4RotationMatrix Rotate25X; Rotate25X.rotateX(25*deg);		
     G4RotationMatrix Rotate65X; Rotate65X.rotateX(65*deg);		
     G4RotationMatrix Rotate90Y; Rotate90Y.rotateY(90*deg);
     G4RotationMatrix Rotate270Y; Rotate270Y.rotateY(-90*deg);
     G4RotationMatrix Rotate90X180Z; Rotate90X180Z.rotateX(90*deg); Rotate90X180Z.rotateZ(180*deg);	
     G4RotationMatrix Rotate90Y180Z; Rotate90Y180Z.rotateY(90*deg); Rotate90Y180Z.rotateZ(180*deg);	
     G4RotationMatrix Rotate90Z; Rotate90Z.rotateZ(90*deg);	
     G4RotationMatrix Rotate90Y180X; Rotate90Y180X.rotateY(90*deg); Rotate90Y180X.rotateX(180*deg);	
     G4RotationMatrix Rotate270Z; Rotate270Z.rotateZ(-90*deg);	
     G4RotationMatrix Rotate270Y180X; Rotate270Y180X.rotateY(-90*deg); Rotate270Y180X.rotateX(180*deg);	
     G4RotationMatrix RotateNull;   
   
     
   G4int i;
   G4double x_pos, y_pos, z_pos;
   G4double dx,dy,dz;
   G4double d_y1,d_y2;
//   G4RotationMatrix* rmx;
   G4double pl_thick;	// z-axis
   G4double pl_width;	// y-axis
   G4double pl_length;	// x-axis
   
   G4LogicalVolume* Volume_log;
   G4VPhysicalVolume* vol_phys;

  G4double GapX			= 0.5*mm;
  G4double GapY			= 0.25*mm;

  G4double ScintThickness	=  7.*mm;
  G4double ScintSizeX		= 79.5*mm;
  G4double ScintSizeZ		= NX_BARS*(ScintSizeX+GapX);

  G4double IronThickness	= 16.*mm;
  G4double IronSizeX		= 16.*cm;
  G4double IronSizeZ		= ScintSizeZ+2.*10.*cm;

  G4int NbOfLayers		= 10;		// Hadron Calo layers */
  G4int NbOfXBars		= NX_BARS;	// number of bars for phi
  G4int NbOfZBars		= NZ_BARS;	// number of bars for theta
        
  G4double LayerStep		= 35.*mm;

  G4double VertPos		= 150.0*cm;	// 150.*cm; !! now to fron face of SANDW+ACC
  G4double HorPos = VertPos*(tan((90.-45.)*deg)+tan((90.-85.)*deg))/2.;

// Compute sizes
  G4double StripSizeX=IronSizeX;//(ScintSizeX+GapX)*2;
  G4double StripSizeZ=IronSizeZ+2*GapX;
  G4double StripSizeY=(ScintThickness+GapY)*2+IronThickness+4*GapY;
  
  G4double LayerSizeX=StripSizeX*NbOfXBars/2;
  G4double LayerSizeY=StripSizeY;
  G4double LayerSizeZ=StripSizeZ;

  G4double SandSizeX=IronSizeZ;
  G4double SandSizeY=LayerStep*(NbOfLayers+1)+(ScintThickness+GapY)*2;	// sandw+ACC
  G4double SandSizeZ=IronSizeZ;

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN WORLD//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    
    // create solidWorld
    G4Box* solidWorld =    
    new G4Box("World",                                  //the name of world
       world_sizeX/2., world_sizeY/2., world_sizeZ/2.); //the size of world
    
    // create LogicWorld 
    G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        material_AIR,                  //its material
                        "World");            //its name
    logicWorld->SetVisAttributes(G4VisAttributes::Invisible);
     
    //create Physical World
    G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(0.,0.,0.),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      -1,                     //copy number
                      true);                 //overlaps checking
    
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END WORLD//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    
    
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN TARGET//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    
G4double w_lng = 24.0*cm; //21.6*cm;		// titanium window length
G4double w_shft=-10.*cm;

G4double w_pos = w_lng/2 + w_shft;

G4Box *VolumeOut_box = 
   new G4Box("VolumeOut_box1", 6.3/2*cm, 5.0/2*cm, 52.8/2*cm);
	
G4Box *VolumeIn_box = 
   new G4Box("VolumeIn_box", (6.3-0.6)/2*cm, (5.0-0.6)/2*cm, 52.81/2*cm);
	
dx=52.8/2.*cm - (w_pos+(w_lng+1.4*cm)/2.)-1.0*cm;
//dz=52.8/2.*cm - 0.5*cm - dx/2.;

d_y1=1.0*cm;		// inlet tube outer radius
d_y2=d_y1-0.2*cm;	// inlet tube inner radius
  
   G4Tubs *InletHole_tub = 
   new G4Tubs("InletHole_tub", 0.0*cm, d_y1, 0.31/2.*cm, 0.*deg, 360.*deg);
   G4Tubs *Inlet_tub = 
   new G4Tubs("Inlet_tub", d_y2, d_y1, 20./2.*cm, 0.*deg, 360.*deg);

   G4SubtractionSolid *Volume_0 = 
   new G4SubtractionSolid("Volume_0", VolumeOut_box, VolumeIn_box);

   G4SubtractionSolid *Volume_1 = 
   new G4SubtractionSolid("Volume_1", Volume_0, InletHole_tub, 
  		G4Transform3D(Rotate90Y, G4ThreeVector(3.0*cm, 0.0*cm, 0.0*cm)));

   G4SubtractionSolid *Volume_3; Volume_3=Volume_1;
	
   G4Box *Cavity_box = 
   new G4Box("Cavity_box", 4.0/2*cm, 5.05/2*cm, w_lng/2);
 
   G4SubtractionSolid *Volume = 
   new G4SubtractionSolid("Volume", Volume_3, Cavity_box, 
 			G4Transform3D(RotateNull, G4ThreeVector(0.0, 0.0, w_pos)));
			
  //Volume_log = new G4LogicalVolume(Volume, material_STEEL, "Volume_log", 0, 0, 0);
    Volume_log = new G4LogicalVolume(Volume, material_STEEL, "Volume_log");

   new G4PVPlacement(0,
                     G4ThreeVector(0.,0.,0.),
                     Volume_log,
                     "Volume_phys",
                     logicWorld,
                     false,
                     -1,
                     true);
    
  Volume_log->SetVisAttributes(color_STEEL);
        
  G4LogicalVolume* InTub_log = new G4LogicalVolume(Inlet_tub, material_ALUMIN, 
						"InTub_log", 0, 0, 0);
  new G4PVPlacement(G4Transform3D(Rotate90Y,
                    //G4ThreeVector((1.2+0.)*cm, 0.0*cm, 0.0*cm)),
                    G4ThreeVector((1.2+10.)*cm, 0.0*cm, 0.0*cm)),  //??????????
                    InTub_log,
                    "InTub_phys",
                    logicWorld,
                    false,
                    -1);

  InTub_log->SetVisAttributes(color_ALUMIN);
			
// Windows
   G4Box *Window_box = new G4Box("Window_box", 5.2/2*cm, 3.0/2*mm, (w_lng+1.4*cm)/2);
   G4LogicalVolume* Window_log = new G4LogicalVolume(Window_box, material_VACUUM, "Window_log", 0, 0, 0);
   
   //Window_log->SetVisAttributes(G4VisAttributes::Invisible);
   
   G4Box *W1b = new G4Box("W1b", 2.0/2*mm, 2.0/2*mm, w_lng/2);
   G4LogicalVolume* W1b_log = new G4LogicalVolume(W1b, material_STEEL, "W1b_log", 0, 0, 0);
   W1b_log->SetVisAttributes(color_STEEL);
	
  new G4PVPlacement(0,
                    G4ThreeVector(2.1*cm, -0.5*mm ,0.0), 
                    W1b_log,
                    "W1b",
                    Window_log,
                    false,
                    -1);
   //vol_phys = new G4PVPlacement(0, G4ThreeVector(-2.1*cm, -0.5*mm ,0.0), W1b_log, "W1b", Window_log, false, -1);

   G4Box *W2b = new G4Box("W2b", 4.4/2*cm, 2.0/2*mm, 3.0/2*mm);
   G4LogicalVolume* W2b_log = new G4LogicalVolume(W2b, material_STEEL, "W2b_log", 0, 0, 0);
   W2b_log->SetVisAttributes(color_STEEL);
	
  new G4PVPlacement(0,
                    G4ThreeVector(0.0, -0.5*mm ,(w_lng+3*mm)/2.), 
                    W2b_log,
                    "W2b",
                    Window_log,
                    false,
                    -1);
  
  new G4PVPlacement(0,
                    G4ThreeVector(0.0, -0.5*mm ,-(w_lng+.3*mm)/2.), 
                    W2b_log,
                    "W2b",
                    Window_log,
                    false,
                    -1);
  
   G4Box *W3b = new G4Box("W3b", 6.0/2*mm, 1.0/2*mm, w_lng/2);
   G4LogicalVolume* W3b_log = new G4LogicalVolume(W3b, material_STEEL, "W3b_log", 0, 0, 0);
   W3b_log->SetVisAttributes(color_STEEL);

  new G4PVPlacement(0,
                    G4ThreeVector(2.3*cm, 1.0*mm ,0.0), 
                    W3b_log,
                    "W3b",
                    Window_log,
                    false,
                    -1);
   
  new G4PVPlacement(0,
                    G4ThreeVector(-2.3*cm, 1.0*mm ,0.0), 
                    W3b_log,
                    "W3b",
                    Window_log,
                    false,
                    -1);

   G4Box *W4b = new G4Box("W4b", 5.2/2*cm, 1.0/2*mm, 7.0/2*mm);
   G4LogicalVolume* W4b_log = new G4LogicalVolume(W4b, material_STEEL, "W4b_log", 0, 0, 0);
   W4b_log->SetVisAttributes(color_STEEL);
	
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, 1.0*mm ,(w_lng+7.*mm)/2.), 
                     W4b_log,
                     "W4b",
                     Window_log,
                     false,
                     -1);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, 1.0*mm ,-(w_lng+7.*mm)/2.), 
  		     W4b_log,
                     "W4b",
                     Window_log,
                     false,
                     -1);


   //G4VPhysicalVolume* Window_phys = 
   new G4PVPlacement(0, 
                     G4ThreeVector(0.0, 2.65*cm,w_pos), 
  		     Window_log,
                     "Volume_phys",
                     logicWorld,
                     false,
                     -1);

   //Window_phys = 
   new G4PVPlacement(G4Transform3D(Rotate180Z, 
                     G4ThreeVector(0.0, -2.65*cm,w_pos)), 
                     Window_log,
                     "Volume_phys",
                     logicWorld,
                     false,
                     -1);

    
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN TITAN FOIL//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
   
   G4Box *TitanFoil_box = 
   new G4Box("TitanFoil_box", 5.2/2*cm, 0.007/2*cm, (w_lng+1.4*cm)/2.);
 
   G4LogicalVolume*
   TitanFoil_log = new G4LogicalVolume(TitanFoil_box, material_TITAN, "TitanFoil_log", 0, 0, 0);	
   new G4PVPlacement(0,
                     G4ThreeVector(0.0*cm, 2.81*cm, w_pos), 
                     TitanFoil_log,
                     "TitanFoil_phys",
                     logicWorld,
                     false,
                     -1);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0*cm, -2.81*cm, w_pos), 
                     TitanFoil_log,
                     "TitanFoil_phys",
                     logicWorld,
                     false,
                     -1);

   TitanFoil_log->SetVisAttributes(color_TITAN);
   
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END TITAN FOIL//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
   
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN STORAGE CELL//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//   #define CELL_THICK 0.05
   G4Tubs *Cell_tube = 
    new G4Tubs("Cell", 13./2.*mm, (13.+0.05)/2.*mm, 40./2.*cm, 90.*deg, 180.*deg);
  // new G4Tubs("Cell", 13./2.*mm, (13.+CELL_THICK)/2.*mm, 40./2.*cm, 90.*deg, 180.*deg);
   
   G4LogicalVolume*
   Cell_log = new G4LogicalVolume(Cell_tube, material_ALUMIN, "Cell_log", 0, 0, 0);
   Cell_log->SetVisAttributes(color_ALUMIN);
   
   new G4PVPlacement(0,
                     G4ThreeVector(-(24.-13.)/2.*mm, 0., 0.), 
                     Cell_log,
                     "Cell",
                     logicWorld,
                     false,
                     -1);	
   
   
   //new G4PVPlacement(&Rotate180Z,
   new G4PVPlacement(&Rotate90Y,
                     G4ThreeVector((24.-13.)/2.*mm, 0., 0.), 
                     Cell_log,
                     "Cell",
                     logicWorld,
                     false,
                     -1);
   
//   G4Box *Cell_plate = new G4Box("Cplate",(24.-13.)/2*mm,CELL_THICK/2*mm,40./2.*cm);	
      G4Box *Cell_plate = new G4Box("Cplate",(24.-13.)/2*mm, 0.05/2*mm,40./2.*cm);	
   
   Cell_log = new G4LogicalVolume(Cell_plate, material_ALUMIN, "Cell2_log", 0, 0, 0);
   Cell_log->SetVisAttributes(color_ALUMIN);
   
   new G4PVPlacement(0,
                     //G4ThreeVector(0.,(13.+CELL_THICK)/2., 0.), 
                     G4ThreeVector(0.,(13.+0.05)/2., 0.), 
                     Cell_log,
                     "Cell",
                     logicWorld,
                     false,
                     -1);	
   
   new G4PVPlacement(0,
                     //G4ThreeVector(0.,-(13.+CELL_THICK)/2., 0.), 
                     G4ThreeVector(0.,-(13.+0.05)/2., 0.), 
                     Cell_log,
                     "Cell",
                     logicWorld,
                     false,
                     -1);	

   
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END STORAGE CELL//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    
   
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END TARGET//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN CHAMBER//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
   G4Box *vbox = 
   new G4Box("VCBox", 12.0/2.*cm, 3.0/2.*cm, 53.0/2.*cm);

   G4LogicalVolume*
   VCBox_log = new G4LogicalVolume(vbox, material_AIR, "VCBox_log", 0, 0, 0);
   VCBox_log->SetVisAttributes(G4VisAttributes::Invisible);

   vbox = new G4Box("frame", 11.4/2.*cm, 0.4/2.*cm, 52.0/2.*cm); 

   G4Box *vcwin = new G4Box("framew", 9.4/2.*cm, 0.4025/2.*cm, 43.6/2.*cm);

   G4SubtractionSolid *VCframe = 
	new G4SubtractionSolid("VCframe_0", vbox, vcwin, 
		G4Transform3D(RotateNull, G4ThreeVector(0., 0., 0.)));

   vbox = new G4Box("stef", 114./2.*mm, 20.0/2.*mm, 48.0/2.*cm); 

   vcwin = new G4Box("stefw", 94./2.*mm,20.02/2.*mm, 43.6/2.*cm);

   G4SubtractionSolid *VCStef = 
   new G4SubtractionSolid("VCframe_0", vbox, vcwin, 
		G4Transform3D(RotateNull, G4ThreeVector(0., 0., 0.)));

   G4LogicalVolume *VC_log = new G4LogicalVolume(VCframe, material_STEEL, "VCFrame_log", 0, 0, 0);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0., 12.0*mm, 0.), 
                     VC_log,
                     "VCFrame_phys",
                     VCBox_log,
                     false,
                     -1);

   VC_log->SetVisAttributes(color_STEEL);

   VC_log = new G4LogicalVolume(VCStef, material_SIO_2, "VCStef_log", 0, 0, 0);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0., 0.0*cm, 0.), 
                     VC_log,
                     "VCStef_phys",
                     VCBox_log,
                     false,
                     -1);

   VC_log->SetVisAttributes(color_SIO_2);

   //vbox =  new G4Box("VCGas_trap", 2.*NVC_WRS/2.*mm,10.0/2.*mm,43.6/2.*cm);
   vbox =  new G4Box("VCGas_trap", 2.*42./2.*mm, 10.0/2.*mm, 43.6/2.*cm);
   VC_log = new G4LogicalVolume(vbox, material_ARCO_2, "VCGas_log", 0, 0, 0);
 
   new G4PVPlacement(0,
                     G4ThreeVector(0.0*cm, 0.0*cm, 0.0*cm), 
                     VC_log,
                     "VCtrap_phys",
                     VCBox_log,
                     false,
                     -1);
                     //VC_IND);

   vbox =  new G4Box("VCGas_cell", 2./2.*mm,10.0/2.*mm,43.6/2.*cm);

   G4LogicalVolume*
   VCGas_log = new G4LogicalVolume(vbox, material_ARCO_2, "VCGas_log", 0, 0, 0);
   //new G4PVReplica("VCcells", VCGas_log, VC_log, kXAxis, NVC_WRS, 2.*mm);
   new G4PVReplica("VCcells", VCGas_log, VC_log, kXAxis, 42, 2.*mm);

   VCGas_log->SetVisAttributes(color_ARCO_2);

   vbox = new G4Box("VCMylar_box1", 94.0/2.*mm, 0.02/2.*mm, 43.6/2.*cm);

   VC_log = new G4LogicalVolume(vbox, material_MYLAR, "VCMylar1_log", 0, 0, 0);

   new G4PVPlacement(0,
                     G4ThreeVector(0.0,-5.01*mm, 0.0), 
                     VC_log,
                     "VCMylar1_phys",
                     VCBox_log,
                     false,
                     -1);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0,  5.01*mm, 0.0), 
                     VC_log,
                     "VCMylar1_phys",
                     VCBox_log,
                     false,
                     -1);


   VC_log->SetVisAttributes(color_MYLAR);

   vbox = new G4Box("VCMylar_box1", 94.0/2.*mm, 0.07/2.*mm, 43.6/2.*cm);

   VC_log = new G4LogicalVolume(vbox, material_MYLAR, "VCMylar1_log", 0, 0, 0);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, -10.0*mm, 0.0), 
                     VC_log,
                     "VCMylar1_phys",
                     VCBox_log,
                     false,
                     -1);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0,  10.0*mm, 0.0), 
                     VC_log,
                     "VCMylar1_phys",
                     VCBox_log,
                     false,
                     -1);

   VC_log->SetVisAttributes(color_MYLAR);

   new G4PVPlacement(G4Transform3D(Rotate180Z,
                     G4ThreeVector(0.0*cm,-7.3*cm, 10.*cm)), 
                     VCBox_log,
                     "VCBox_phys",
                     logicWorld,
                     false,
                     -1);
                     //ARM1_IND);
   
G4LogicalVolume* WCTheta1_gas=NULL;
G4LogicalVolume* WCTheta1 = ConstructWC(NW1_WRS*2.*cm, 20.*cm, WC1_IND, WCTheta1_gas); 

G4LogicalVolume* WCPhi1_gas=NULL;
G4LogicalVolume* WCPhi1 = ConstructWC(NW2_WRS*2.*cm, 45.*cm, WC2_IND, WCPhi1_gas); 

G4LogicalVolume* WCTheta2_gas=NULL;
G4LogicalVolume* WCTheta2 = ConstructWC(NW3_WRS*2.*cm, 43.*cm, WC3_IND, WCTheta2_gas); 
   
   new G4PVPlacement(G4Transform3D(Rotate180Z, G4ThreeVector(0.0*cm,-7.3*cm, 10.*cm)), 
 			VCBox_log, "VCBox_phys", logicWorld, false, -1);//ARM1_IND);
   new G4PVPlacement(G4Transform3D(Rotate180Z,	
   			G4ThreeVector(0.0*cm, -15.0*cm, 9.0*cm)), 
			WCTheta1, "WCTheta1a", logicWorld, false, -1);//ARM1_IND);
   new G4PVPlacement(G4Transform3D(Rotate180Z,	
   			G4ThreeVector(0.0*cm, -35.0*cm, 20.0*cm)), 
			WCTheta2, "WCTheta2a", logicWorld, false, -1);//ARM1_IND);
   new G4PVPlacement(G4Transform3D(Rotate90Y180Z,	
   			G4ThreeVector(0.0*cm, -25.0*cm, 14.5*cm)), 
			WCPhi1, "WCPhi1a", logicWorld, false, -1);//ARM1_IND);   
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END CHAMBER//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//   
   
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN AC//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
   
   pl_thick=1.0*cm; //2.0*cm;	// y-axis
   pl_width=100.0*cm;	// z-axis
   pl_length=60.0*cm;	// x-axis

   G4Box* ubox = new G4Box("AC_box", 300.0/2.*cm, (pl_thick+0.4*mm)/2.,(pl_width+1*cm)/2.);
   //G4LogicalVolume *
   ACBox_log = new G4LogicalVolume(ubox, material_AIR, "ACBox_log", 0, 0, 0);
   ACBox_log->SetVisAttributes(G4VisAttributes::Invisible);

   ubox = new G4Box("AC", pl_length/2., pl_thick/2., pl_width/2.);
   //G4LogicalVolume* 
   AC_log = new G4LogicalVolume(ubox, material_SCINTIL, "AC_log", 0, 0, 0);
   AC_log->SetVisAttributes(color_SCINTIL);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, 0.0, 0.0), 
                     AC_log,
                     "AC",
                     ACBox_log,
                     false,
                     -1);
//                     AC_IND);

//   G4VisAttributes *ProCover_VisAtt = new G4VisAttributes(blackpaper_col);
   ubox = new G4Box("ACCover", pl_length/2., 0.15/2.*mm, pl_width/2.);
   G4LogicalVolume *ACCover_log = new G4LogicalVolume(ubox, material_MYLAR, "ACCover_log", 0, 0, 0);
//   ACCover_log->SetVisAttributes(ProCover_VisAtt);
//   ACCover_log->SetVisAttributes(new G4VisAttributes(G4Color(0.1,0.1,0.5)));
   ACCover_log->SetVisAttributes(color_MYLAR);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, +pl_thick/2.+0.1*mm, 0.0), 
                     ACCover_log,
                     "ACCover_phys",
                     ACBox_log,
                     false,
                     -1);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, -pl_thick/2.-0.1*mm, 0.0), 
                     ACCover_log,
                     "ACCover_phys",
                     ACBox_log,
                     false,
                     -1);

   // PLACE AC    
// Position -- close to 3rd Drift Chamber 
x_pos=0.0; y_pos=43.5*cm; 
z_pos= y_pos*(tan((90.-45.)*deg)+tan((90.-85.)*deg))/2.; 
//G4cerr << "AC : y_pos="<<y_pos/cm<<" cm  z_pos="<<z_pos/cm<<" cm"<<"*********************************"<<G4endl;
  
   new G4PVPlacement(G4Transform3D(RotateNull, 
                     G4ThreeVector(x_pos, -y_pos, z_pos)), 
                     ACBox_log,
                     "AC_phys",
                     logicWorld,
                     false,
                     0);
//                     ARM1_IND);
    new G4PVPlacement(G4Transform3D(RotateNull, 
                     G4ThreeVector(x_pos, -y_pos-1.*cm-0.4*mm, z_pos)), 
                     ACBox_log,
                     "AC_phys",
                     logicWorld,
                     false,
                     1);
//                     ARM1_IND);
   
   new G4PVPlacement(G4Transform3D(Rotate180Z, 
                     G4ThreeVector(x_pos, y_pos, z_pos)), 
                     ACBox_log,
                     "AC_phys",
                     logicWorld,
                     false,
                     2);
//                     ARM2_IND);
   new G4PVPlacement(G4Transform3D(Rotate180Z, 
                     G4ThreeVector(x_pos, y_pos+1.*cm+0.4*mm, z_pos)), 
                     ACBox_log,
                     "AC_phys",
                     logicWorld,
                     false,
                     3);
//  

   
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END AC//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//



    // RPC
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN RPC// for simulated mRPC
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

pl_thick=1.0*cm;	// y-axis
pl_width=SandSizeZ;	// z-axis
pl_length=SandSizeX;	// x-axis

//G4Material* rpc_material=Scintil;

   ubox = new G4Box("RPC", pl_length/2., pl_thick/2., pl_width/2.);
   G4LogicalVolume *RPC_log = new G4LogicalVolume(ubox, material_SCINTIL, "RPC_log", 0, 0, 0);
   RPC_log->SetVisAttributes(color_SCINTIL);

// PLACE RPC    
// Position -- close to HS
x_pos=0.0; 
y_pos=VertPos-0.5*LayerSizeY;
//G4cout<<"*** RPC_Y = "<<y_pos<<G4endl;
z_pos= y_pos*(tan((90.-45.)*deg)+tan((90.-85.)*deg))/2.; 
//G4cerr << "========> AC : y_pos="<<y_pos/cm<<" cm  z_pos="<<z_pos/cm<<" cm"<<G4endl;

//#ifdef RPC
/*
i=TOF_IND  + ARM1_IND;
new G4PVPlacement(G4Transform3D(RotateNull, 
   		  G4ThreeVector(x_pos, -y_pos, z_pos)), 
		  RPC_log,
                  "RPC1_phys",
                  logicWorld,
                  false,
                  i);
*/
/*
i=TOF_IND  + ARM2_IND;
new G4PVPlacement(G4Transform3D(Rotate180Z, 
   		  G4ThreeVector(x_pos,  y_pos, z_pos)), 
		  RPC_log,
                  "RPC2_phys",
                  logicWorld,
                  false,
                  -1);
//                  i);
*/
//#endif
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END RPC// for simulated mRPC
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//




					
//------------------------------------
//         HARDRON SANDWICH        //////
//------------------------------------
// FEU
/*
   G4LogicalVolume *lFEU;

   G4Tubs *FEU30_box = new G4Tubs("FEU30",0.0*cm,4.2*cm,25.2/2.0*cm,0.,2.0*M_PI);
   G4LogicalVolume *FEU30_log = 
   		new G4LogicalVolume(FEU30_box,material_AIR,"FEU30_log",0,0,0);
   FEU30_log->SetVisAttributes(G4VisAttributes::Invisible);

   G4Tubs *FEU30_tub = new G4Tubs("FEU30tub",3.0*cm,4.0*cm,25.0/2.0*cm,0.,2.0*M_PI);
   lFEU=new G4LogicalVolume(FEU30_tub,material_STEEL,"FEU30t_log",0,0,0);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.,0.,0.1*cm), 
                     lFEU,
                     "t",
                     FEU30_log,
                     false,
                     -1);
   
   G4Tubs *FEU30_lid = new G4Tubs("FEU30lid",0.0,4.0*cm,0.1/2.0*cm,0.,2.0*M_PI);
   lFEU=new G4LogicalVolume(FEU30_lid, material_STEEL, "FEU30l_log", 0,0,0);
   new G4PVPlacement(0,
                     G4ThreeVector(0.,0.,+(25.0/2+0.1)*cm), 
 		     lFEU,
                     "l",
                     FEU30_log,
                     false,
                     -1);

   

   G4Tubs *FEU63_box = new G4Tubs("FEU63",0.0*cm,8.2*cm,35.2/2.0*cm,0.,2.0*M_PI);
   G4LogicalVolume *FEU63_log = 
   new G4LogicalVolume(FEU63_box, material_AIR,"FEU63_log",0,0,0);
   FEU63_log->SetVisAttributes(G4VisAttributes::Invisible);

   G4Tubs *FEU63_tub = new G4Tubs("FEU63tub",7.0*cm,8.0*cm,35.0/2.0*cm,0.,2.0*M_PI);
   lFEU=new G4LogicalVolume(FEU63_tub,material_STEEL,"FEU63t_log",0,0,0);
   lFEU->SetVisAttributes(color_STEEL);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.,0.,0.1*cm), 
                     lFEU,
                     "t",
                     FEU63_log,
                     false,
                     -1);
   
   G4Tubs *FEU63_lid = new G4Tubs("FEU63lid",0.0,8.0*cm,0.1/2.0*cm,0.,2.0*M_PI);
   lFEU=new G4LogicalVolume(FEU63_lid, material_STEEL,"FEU63l_log",0,0,0);
   lFEU->SetVisAttributes(color_STEEL);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.,0.,+(35.0/2+0.1)*cm), 
                     lFEU,
                     "l",
                     FEU63_log,
                     false,
                     -1);

//   G4double FEUlength=35.*cm;
*/

////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/// SANDWICH
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////

//         
//        Y^
//         |
//         +---->Z
//       /        
//    X|_

// in this version sandwich is square with all strips (X and Z) are the same
// and number of X bars equals to number of Z bars

  /* 
  G4double GapX			= 0.5*mm;
  G4double GapY			= 0.25*mm;

  G4double ScintThickness	=  7.*mm;
	   ScintSizeX		= 79.5*mm;
	   ScintSizeZ		= NX_BARS*(ScintSizeX+GapX);

  G4double IronThickness	= 16.*mm;
  G4double IronSizeX		= 16.*cm;
  G4double IronSizeZ		= ScintSizeZ+2.*10.*cm;

  G4int NbOfLayers		= 10;		// Hadron Calo layers 
        NbOfXBars		= NX_BARS;	// number of bars for phi
        NbOfZBars		= NZ_BARS;	// number of bars for theta
        
  G4double LayerStep		= 35.*mm;

  G4double VertPos		= 150.0*cm;	// 150.*cm; !! now to fron face of SANDW+ACC
  G4double HorPos = VertPos*(tan((90.-45.)*deg)+tan((90.-85.)*deg))/2.;

// Compute sizes
  G4double StripSizeX=IronSizeX;//(ScintSizeX+GapX)*2;
  G4double StripSizeZ=IronSizeZ+2*GapX;
  G4double StripSizeY=(ScintThickness+GapY)*2+IronThickness+4*GapY;
  
  G4double LayerSizeX=StripSizeX*NbOfXBars/2;
  G4double LayerSizeY=StripSizeY;
  G4double LayerSizeZ=StripSizeZ;

  G4double SandSizeX=IronSizeZ;
  G4double SandSizeY=LayerStep*(NbOfLayers+1)+(ScintThickness+GapY)*2;	// sandw+ACC
  G4double SandSizeZ=IronSizeZ;
*/
  ubox = new G4Box("Sand", SandSizeX/2.,SandSizeY/2.,SandSizeZ/2.);
  G4LogicalVolume* sand_vol = new G4LogicalVolume(ubox, material_AIR, "Sand", 0,0,0);
  sand_vol->SetVisAttributes(G4VisAttributes::Invisible);

  
  
  
  ubox = new G4Box("Absor", StripSizeX/2.,IronThickness/2.,IronSizeZ/2.);
  G4LogicalVolume* absor_vol = new G4LogicalVolume(ubox, material_STEEL, "Absor", 0,0,0);
  absor_vol->SetVisAttributes(color_STEEL);

  ubox = new G4Box("Scint", ScintSizeX/2.,ScintThickness/2.,ScintSizeZ/2.);
  //G4LogicalVolume*
  scint_log = new G4LogicalVolume(ubox, material_SCINTIL, "Scint", 0,0,0);
  scint_log->SetVisAttributes(color_SCINTIL);

  ubox = new G4Box("Strip", StripSizeX/2.,StripSizeY/2.,StripSizeZ/2.);
  G4LogicalVolume* strip_log = new G4LogicalVolume(ubox, material_AIR, "Strip", 0,0,0);
  strip_log->SetVisAttributes(G4VisAttributes::Invisible);

// Standard layer : iron bar and 2 pairs of scints above and below  
// iron bar  
  new G4PVPlacement(0,
                    G4ThreeVector(0.,0.,0.), 
                    absor_vol,
                    "BarI",
                    strip_log,
                    false,
                    -1);
  
  y_pos=(IronThickness+ScintThickness)/2.+GapY;
  x_pos=(ScintSizeX+GapX)/2.;
// 2 scint strips on bottom of  iron bar   
  new G4PVPlacement(0,
                    G4ThreeVector(-x_pos,-y_pos,0.), 
		    scint_log,
                    "BarS",
                    strip_log,
                    false,
                    0);
  
  new G4PVPlacement(0,
                    G4ThreeVector(x_pos,-y_pos,0.), 
		    scint_log,
                    "BarS",
                    strip_log,
                    false,
                    1);
//                    N_UNITS);
// 2 scint strips on top of  iron bar   
  new G4PVPlacement(0,
                    G4ThreeVector(-x_pos,y_pos,0.), 
		    scint_log,
                    "BarS",
                    strip_log,
                    false,
                    0);
  //                  2*N_UNITS);
  
  new G4PVPlacement(0,
                    G4ThreeVector(x_pos,y_pos,0.), 
		    scint_log,
                    "BarS",
                    strip_log,
                    false,
                    1);
//                    3*N_UNITS);
  
  ubox = new G4Box("Layer", LayerSizeX/2.,LayerSizeY/2.,LayerSizeZ/2.);
  G4LogicalVolume* layer_log = new G4LogicalVolume(ubox, material_AIR, "Layer", 0,0,0);
  layer_log->SetVisAttributes(G4VisAttributes::Invisible);
  
  new G4PVReplica("Bars",strip_log,layer_log, kXAxis, N_UNITS, StripSizeX);

// half-layer
  dy=IronThickness+GapY+ScintThickness;
  ubox = new G4Box("StripH", StripSizeX/2.,dy/2.,StripSizeZ/2.);
  G4LogicalVolume* stripH_log = new G4LogicalVolume(ubox, material_AIR, "StripH", 0,0,0);
  stripH_log->SetVisAttributes(G4VisAttributes::Invisible);

  dy=(dy-IronThickness)/2.;
  y_pos=dy;
  
  new G4PVPlacement(0,
                    G4ThreeVector(0.,y_pos,0),
                    absor_vol,
                    "BarI",
                    stripH_log,
                    false,
                    -1);
  
  y_pos-=IronThickness/2.;
  y_pos-=GapY;
  y_pos-=ScintThickness/2.;
  x_pos=(ScintSizeX+GapX)/2.;
  
  new G4PVPlacement(0,
                    G4ThreeVector(-x_pos,y_pos,0.), 
                    scint_log,
                    "BarS",
                    stripH_log,
                    false,
                   0);
  
  new G4PVPlacement(0,
                    G4ThreeVector(x_pos,y_pos,0.), 
                    scint_log,
                    "BarS",
                    stripH_log,
                    false,
                    1);
//                    N_UNITS);
  
  ubox = new G4Box("LayerH", LayerSizeX/2.,LayerSizeY/2.,LayerSizeZ/2.);
  G4LogicalVolume* layerH_log = new G4LogicalVolume(ubox, material_AIR, "LayerH", 0,0,0);
  layerH_log->SetVisAttributes(G4VisAttributes::Invisible);
  
  new G4PVReplica("BarsH",stripH_log,layerH_log, kXAxis, N_UNITS, StripSizeX);
  
// outer (iron-less) half-layer  
  ubox = new G4Box("StripO", StripSizeX/2.,StripSizeY/2.,StripSizeZ/2.);
  G4LogicalVolume* stripO_log = new G4LogicalVolume(ubox, material_AIR, "StripO", 0,0,0);
  stripO_log->SetVisAttributes(G4VisAttributes::Invisible);

  y_pos=-StripSizeY/2.;
  y_pos+=ScintThickness/2.;
  x_pos=(ScintSizeX+GapX)/2.;
  
  new G4PVPlacement(0,
                    G4ThreeVector(-x_pos,y_pos,0.), 
		    scint_log,
                    "BarS",
                    stripO_log,
                    false,
                    0);
  
  new G4PVPlacement(0,
                    G4ThreeVector(x_pos,y_pos,0.), 
                    scint_log,
                    "BarS",
                    stripO_log,
                    false,
                    1);  
//                  N_UNITS);
                    
  
  y_pos+=ScintThickness/2.;
  y_pos+=IronThickness/2.;
// NO IRON HERE !!
//  vol_phys = new G4PVPlacement(0, G4ThreeVector(0.,y_pos,0),absor_vol, "BarI", stripH_log, false, 0);
  
  ubox = new G4Box("LayerO", LayerSizeX/2.,LayerSizeY/2.,LayerSizeZ/2.);
  G4LogicalVolume* layerO_log = new G4LogicalVolume(ubox, material_AIR, "LayerO", 0,0,0);
  layerO_log->SetVisAttributes(G4VisAttributes::Invisible);
  
  new G4PVReplica("BarsO",stripO_log,layerO_log, kXAxis, N_UNITS, StripSizeX);

///
  G4int nx=HCX_IND, nz=HCZ_IND;
  
  y_pos=-SandSizeY/2.;

  
  //
  #ifdef ACCC
  
  y_pos+=ScintThickness/2.+GapY;
  x_pos=-NbOfXBars/4.*IronSizeX+IronSizeX/2.+IronSizeX/4.;
  
  for(i=0;i<N_UNITS;i++,x_pos+=IronSizeX){
    vol_phys = new G4PVPlacement(G4Transform3D(Rotate90Y180Z,  G4ThreeVector(0,y_pos,x_pos-(ScintSizeX+GapX)/2.)), 
			scint_log, "ACBar", sand_vol, false, -1);//DE_IND);
  if(i<N_UNITS-1)			
    vol_phys = new G4PVPlacement(G4Transform3D(Rotate90Y180Z,  G4ThreeVector(0,y_pos,x_pos+(ScintSizeX+GapX)/2.)), 
			scint_log, "ACBar", sand_vol, false, -1);//DE_IND);
  }  

  x_pos=-NbOfXBars/4.*IronSizeX+IronSizeX/2.;
  y_pos+=ScintThickness+GapY;

  for(i=0;i<N_UNITS;i++,x_pos+=IronSizeX){
    vol_phys = new G4PVPlacement(G4Transform3D(Rotate90Y180Z,  G4ThreeVector(0,y_pos,x_pos-(ScintSizeX+GapX)/2.)), 
			scint_log, "ACBar", sand_vol, false, -1);//DE_IND);    
    vol_phys = new G4PVPlacement(G4Transform3D(Rotate90Y180Z,  G4ThreeVector(0,y_pos,x_pos+(ScintSizeX+GapX)/2.)), 
			scint_log, "ACBar", sand_vol, false, -1);//DE_IND);    
  }
  
  #endif
  
  
  
  y_pos+=(IronThickness+ScintThickness)/2.+GapY;

  
  
  
  for(i=0;i<NbOfLayers+1;y_pos+=LayerStep,i++){
    if((i&1)==1){	// X-layer
     if(i!= NbOfLayers){	// not last layer? 
      vol_phys =  new G4PVPlacement(G4Transform3D(RotateNull, G4ThreeVector(0.,y_pos,0.)),
			layer_log, "BarsX", sand_vol, false, 0);//nx);
      nx+=NbOfXBars*2;
     }else{	// if last layer - use half-layer
      vol_phys = new G4PVPlacement(G4Transform3D(RotateNull, G4ThreeVector(0.,y_pos,0.)), 
			layerO_log, "BarsHX", sand_vol, false, 0);//nx);
      nx+=NbOfXBars;
     }  
    }else{		// Z-layer
      if(i==0){		// first layer ?
      vol_phys = new G4PVPlacement(G4Transform3D(Rotate90Y180Z, G4ThreeVector(0.,y_pos+dy,0.)), 
			layerH_log, "BarsHZ", sand_vol, false, 22);//nz);
        nz+=NbOfZBars;	
	continue;
      }
      if(i!= NbOfLayers){	// not last layer?
       vol_phys = new G4PVPlacement(G4Transform3D(Rotate90Y180Z, G4ThreeVector(0.,y_pos,0.)), 
			layer_log, "BarsZ", sand_vol, false, 22);//nz);
       nz+=NbOfZBars*2;
     }else{	// if last layer - use half-layer
       vol_phys = new G4PVPlacement(G4Transform3D(Rotate90Y, G4ThreeVector(0.,y_pos,0.)), 
			layerO_log, "BarsOZ", sand_vol, false, 22);//nz);
       nz+=NbOfZBars;
     }  
    }
  }

//G4cerr<<"***** nx="<<nx-HCX_IND<<" nz="<<nz-HCZ_IND;
//G4cerr<<" HCX_IND="<<HCX_IND<<" HCZ_IND="<<HCZ_IND<<" LQ_IND="<<LQ_IND<<G4endl;
   
// Place sandwitches
   y_pos = VertPos+SandSizeY/2.;
   /*
   new G4PVPlacement(G4Transform3D(Rotate180Z, G4ThreeVector(0.0*cm,-y_pos, HorPos)), 
 			sand_vol, "Sand_phys", logicWorld, false, ARM1_IND);
   */
 
   new G4PVPlacement(G4Transform3D(RotateNull, G4ThreeVector(0.0*cm, y_pos, HorPos)), 
 			sand_vol, "Sand_phys", logicWorld, false, -1);//ARM2_IND);


   
       //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN PLASTIC SCINTILLATORS FOR LOW ARM//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
   pl_length=100.0*cm;	// x-axis
   pl_thick=20.0*cm;	// y-axis
   pl_width=20.0*cm;	// z-axis
   
   ubox = new G4Box("PL_box", (pl_length+10.0*cm)/2., (pl_thick+10.0*cm)/2.,(pl_width+0.02*cm)/2.);
   //G4LogicalVolume *
   PLBox_log = new G4LogicalVolume(ubox, material_AIR, "PLBox_log", 0, 0, 0);
   PLBox_log->SetVisAttributes(G4VisAttributes::Invisible);

   ubox = new G4Box("PL", pl_length/2., pl_thick/2., pl_width/2.);
   //G4LogicalVolume *
   PL_log = new G4LogicalVolume(ubox, material_SCINTIL, "PL_log", 0, 0, 0);
   PL_log->SetVisAttributes(color_SCINTIL);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, 0.0, 0.0), 
                     PL_log,
                     "PL",
                     PLBox_log,
                     false,
                     -1);

   ubox = new G4Box("PLCover", pl_length/2., 0.01/2.*cm, pl_width/2.);
   G4LogicalVolume *PLCover_log = new G4LogicalVolume(ubox, material_MYLAR, "PLCover_log", 0, 0, 0);
   PLCover_log->SetVisAttributes(color_MYLAR);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, pl_thick/2.+0.005*cm, 0.0), 
                     PLCover_log,
                     "PLCover_phys",
                     PLBox_log,
                     false,
                     -1);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, -pl_thick/2.-0.005*cm, 0.0), 
                     PLCover_log,
                     "PLCover_phys",
                     PLBox_log,
                     false,
                     -1);
   
   ubox = new G4Box("PLCover", pl_length/2., pl_thick/2, 0.01/2.*cm);
   PLCover_log = new G4LogicalVolume(ubox, material_MYLAR, "PLCover_log", 0, 0, 0);
   PLCover_log->SetVisAttributes(color_MYLAR);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, 0.0, -pl_width/2.-0.005*cm), 
                     PLCover_log,
                     "PLCover_phys",
                     PLBox_log,
                     false,
                     -1);
   
   new G4PVPlacement(0,
                     G4ThreeVector(0.0, 0.0, pl_width/2.+0.005*cm), 
                     PLCover_log,
                     "PLCover_phys",
                     PLBox_log,
                     false,
                     -1);
   // PLACE PROTON PLASTIC FOR LOW ARM  
x_pos=0.0; y_pos=106.*cm; 
z_pos= -4.0; 

   new G4PVPlacement(G4Transform3D(RotateNull,  
                     G4ThreeVector(x_pos, -y_pos, z_pos)), // 1-st plastic
                     PLBox_log,
                     "PL_phys",
                     logicWorld,
                     false,
                     0);
  
   new G4PVPlacement(G4Transform3D(RotateNull,  
                     G4ThreeVector(x_pos, -y_pos, z_pos+pl_width+2.*0.01*cm)), // 2-nd plastic
                     PLBox_log,
                     "PL_phys",
                     logicWorld,
                     false,
                     1);

  new G4PVPlacement(G4Transform3D(RotateNull,  
                     G4ThreeVector(x_pos, -y_pos, z_pos+2.*pl_width+4.*0.01*cm)), // 3-d plastic
                     PLBox_log,
                     "PL_phys",
                     logicWorld,
                     false,
                     2);   
  
  new G4PVPlacement(G4Transform3D(RotateNull,  
                     G4ThreeVector(x_pos, -y_pos, z_pos+3.*pl_width+6.*0.01*cm)), // 4-s plastic
                     PLBox_log,
                     "PL_phys",
                     logicWorld,
                     false,
                     3);   
  
  new G4PVPlacement(G4Transform3D(RotateNull,  
                     G4ThreeVector(x_pos, -y_pos, z_pos+4.*pl_width+8.*0.01*cm)), // 5-th plastic
                     PLBox_log,
                     "PL_phys",
                     logicWorld,
                     false,
                     4); 

new G4PVPlacement(G4Transform3D(RotateNull,  
                     G4ThreeVector(x_pos, -y_pos, z_pos+5.*pl_width+10.*0.01*cm)), // 6-th plastic
                     PLBox_log,
                     "PL_phys",
                     logicWorld,
                     false,
                     5);  
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END PLASTIC SCINTILLATORS FOR LOW ARM//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //BEGIN ELECTRON DETECTOR FOR LQ-POLARIMETR//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    x_pos=0.0; y_pos=25.*cm; z_pos=37.6*cm; 
    
    ubox = new G4Box("PLSTR_box", 50.*cm/2., 23.5*cm/2.,2.0*cm/2.);
    PLSTR_log = new G4LogicalVolume(ubox, material_SCINTIL, "PLSTR_log", 0, 0, 0);
    PLSTR_log->SetVisAttributes(color_SCINTIL);
    new G4PVPlacement(G4Transform3D(Rotate325X,  
                     G4ThreeVector(x_pos, y_pos, z_pos)), // Place plastic polystyrol
                     PLSTR_log,
                     "PLSTR_phys",
                     logicWorld,
                     false,
                     0);

    ubox = new G4Box("DR_box", 50.*cm/2., 28.*cm/2.,0.5*cm/2.);
    G4LogicalVolume *DR_log = new G4LogicalVolume(ubox, material_ALUMIN, "DR_log", 0, 0, 0);
    DR_log->SetVisAttributes(color_ALUMIN);
    new G4PVPlacement(G4Transform3D(Rotate325X,  
                     G4ThreeVector(x_pos, y_pos, z_pos-1.3*cm)), // Place dural'
                     DR_log,
                     "DR_phys",
                     logicWorld,
                     false,
                     0);
    
    ubox = new G4Box("Str_box", 50.*cm/2., 23.5*cm/2.,2.0*cm/2.);
    G4LogicalVolume *Str_e_log = new G4LogicalVolume(ubox, material_AIR, "Str_e_log", 0, 0, 0);
    Str_e_log->SetVisAttributes(G4VisAttributes::Invisible);
    ubox = new G4Box("Strip_box", 50.*cm/2., 4.0*cm/2.,0.7*cm/2.);
    Strip_e_log = new G4LogicalVolume(ubox, material_SCINTIL, "Strip_e_log", 0, 0, 0);
    new G4PVPlacement(0,G4ThreeVector(0.0,-10.0*cm, 0.5*cm),Strip_e_log,"Str_e_log",Str_e_log,false,0);
    new G4PVPlacement(0,G4ThreeVector(0.0,-5.0*cm, 0.5*cm),Strip_e_log,"Str_e_log",Str_e_log,false,2);
    new G4PVPlacement(0,G4ThreeVector(0.0, 0.0, 0.5*cm),Strip_e_log,"Str_e_log",Str_e_log,false,4);
    new G4PVPlacement(0,G4ThreeVector(0.0,5.0*cm, 0.5*cm),Strip_e_log,"Str_e_log",Str_e_log,false,6);
    new G4PVPlacement(0,G4ThreeVector(0.0,10.0*cm, 0.5*cm),Strip_e_log,"Str_e_log",Str_e_log,false,8);
    new G4PVPlacement(0,G4ThreeVector(0.0,-7.5*cm, -0.2*cm),Strip_e_log,"Str_e_log",Str_e_log,false,1);
    new G4PVPlacement(0,G4ThreeVector(0.0,-2.5*cm, -0.2*cm),Strip_e_log,"Str_e_log",Str_e_log,false,3);
    new G4PVPlacement(0,G4ThreeVector(0.0,2.5*cm, -0.2*cm),Strip_e_log,"Str_e_log",Str_e_log,false,5);
    new G4PVPlacement(0,G4ThreeVector(0.0,7.5*cm, -0.2*cm),Strip_e_log,"Str_e_log",Str_e_log,false,7);
    
    new G4PVPlacement(G4Transform3D(Rotate325X,  
                     G4ThreeVector(x_pos, y_pos-1*cm, z_pos-2.6*cm)), // Place electron-Strip
                     Str_e_log,
                     "Str_e_log",
                     logicWorld,
                     false,
                     0);
    
    
    ubox = new G4Box("Pb_box", 50.*cm/2., 28.*cm/2.,1.6*cm/2.);
    G4LogicalVolume *Pb_log = new G4LogicalVolume(ubox, material_Pb, "Pb_log", 0, 0, 0);
    Pb_log->SetVisAttributes(color_Pb);
    new G4PVPlacement(G4Transform3D(Rotate325X,  
                     G4ThreeVector(x_pos, y_pos-0.7*cm, z_pos-4.5*cm)), // Place Pb
                     Pb_log,
                     "Pb_phys",
                     logicWorld,
                     false,
                     0);
    
    ubox = new G4Box("Pb_box", 50.*cm/2., 25.*cm/2.,3.0*cm/2.);
    G4LogicalVolume *Plex_log = new G4LogicalVolume(ubox, material_PLEXIGLASS, "Plex_log", 0, 0, 0);
    Plex_log->SetVisAttributes(color_PLEXIGLASS);
    new G4PVPlacement(G4Transform3D(Rotate325X,  
                     G4ThreeVector(x_pos, y_pos-1.5*cm, z_pos-7.8*cm)), // Place dural'
                     Plex_log,
                     "Plex_phys",
                     logicWorld,
                     false,
                     0);
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //END ELECTRON DETECTOR FOR LQ-POLARIMETR//
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//


  fScoringVolume = logicWorld;      
  //
  //always return the physical World
  //
  return physWorld;
}


void DetectorConstruction::ConstructSDandField()
{
  //BEGIN sensitive detector for proton plastic
  G4String ProtonPlasticSDname = "ProtonPlasticSD";
  ProtonPlasticSD* aProtonPlasticSD = new ProtonPlasticSD(ProtonPlasticSDname);
  G4SDManager::GetSDMpointer()->AddNewDetector(aProtonPlasticSD);
  PL_log->SetSensitiveDetector(aProtonPlasticSD);
  //END sensitive detector for proton plastic
  
  //BEGIN sensitive detector for AC
  G4String ACplasticSDname = "ACplasticSD";
  ACplasticSD* aACplasticSD = new ACplasticSD(ACplasticSDname);
  G4SDManager::GetSDMpointer()->AddNewDetector(aACplasticSD);
  AC_log->SetSensitiveDetector(aACplasticSD);
  //END sensitive detector for AC
  
  //BEGIN sensitive detector for strip plastic
  G4String StripPlasticSDname = "StripPlasticSD";
  StripPlasticSD* aStripPlasticSD = new StripPlasticSD(StripPlasticSDname);
  G4SDManager::GetSDMpointer()->AddNewDetector(aStripPlasticSD);
  scint_log->SetSensitiveDetector(aStripPlasticSD);
  //END sensitive detector for strip plastic
  
  //BEGIN sensitive detector for electron strip plastic
  G4String ElectronStripPlasticSDname = "ElectronStripPlasticSD";
  ElectronStripPlasticSD* aElectronStripPlasticSD = new ElectronStripPlasticSD(ElectronStripPlasticSDname);
  G4SDManager::GetSDMpointer()->AddNewDetector(aElectronStripPlasticSD);
  Strip_e_log->SetSensitiveDetector(aElectronStripPlasticSD);
  //END sensitive detector for electron strip plastic
  
  
  //BEGIN sensitive detector for electron polystirol
  G4String ElectronPolystirolSDname = "ElectronPolystirolSD";
  ElectronPolystirolSD* aElectronPolystirolSD = new ElectronPolystirolSD(ElectronPolystirolSDname);
  G4SDManager::GetSDMpointer()->AddNewDetector(aElectronPolystirolSD);
  PLSTR_log->SetSensitiveDetector(aElectronPolystirolSD);
  //END sensitive detector for electron polystirol



}


G4LogicalVolume* DetectorConstruction::ConstructWC(G4double Lwin,
				 G4double Wwin, G4int ind, G4LogicalVolume*& forSD)
{
////////////////////////////////////////////////////////////////////////////////
// VOLUME : Tracking chamber
////////////////////////////////////////////////////////////////////////////////
//         
//        Y^
//         |
//         +---->Z
//       /        
//    X|_
    G4NistManager* nist = G4NistManager::Instance();
    
   // G4Material* material_STEEL = nist->FindOrBuildMaterial("G4_STAINLESS-STEEL"); //material of the STEEL
    auto color_STEEL = new G4VisAttributes(G4Colour(0.0,0.0,1.0));   // color of STEEL
    
    G4Material* material_SIO_2 = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE"); //material of the SIO_2
    auto color_SIO_2 = new G4VisAttributes(G4Colour(0.742, 0.699, 0.121));   // color of SIO_2

    G4Material* material_CO_2 = nist->FindOrBuildMaterial("G4_CARBON_DIOXIDE"); //material of the CO_2
    G4Material* material_AR = nist->FindOrBuildMaterial("G4_Ar"); //material of the Ar

    G4Material* material_ARCO_2 = new G4Material("ARCO_2", 1.98e-03*g/cm3, 2);
    material_ARCO_2->AddMaterial(material_AR, .2);
    material_ARCO_2->AddMaterial(material_CO_2, .8);
    //auto color_ARCO_2 = new G4VisAttributes(G4Colour(0.93, 0.77, 0.57));   // color of ARCO_2
    
//    G4Material* material_VACUUM = nist->FindOrBuildMaterial("G4_Galactic"); //material of the VACUUM

    G4Material* material_MYLAR = nist->FindOrBuildMaterial("G4_MYLAR"); //material of the MYLAR
    auto color_MYLAR = new G4VisAttributes(G4Colour(0.5, 0.2, 0.2));   // color of MYLAR    

G4double Lframe, Wframe, Gap, Swid;
G4double x_pos,y_pos,z_pos;

//Lwin=40.*cm; Wwin=15.*cm; 
Swid=4.*cm; Gap=6.*mm;
Lframe=Lwin+Swid*2;
Wframe=Wwin+Swid*2;

G4double thk=Gap*8.;
G4double foil_thk=0.03*mm;
G4double mylar_thk=0.05*mm;



   G4Box *WCBox_box = 
   new G4Box("WCBox_box", Wframe/2., thk/2., Lframe/2.);

   G4LogicalVolume* 
   WCBox_log = new G4LogicalVolume(WCBox_box, material_ARCO_2, "WCBox_log", 0, 0, 0);
   WCBox_log->SetVisAttributes(G4VisAttributes::Invisible);
	
//------------------------------------------------------------------------------

   G4Box*
   WCBox = new G4Box("WC_S1", Swid/2., thk/2., Lwin/2.);
   G4LogicalVolume* 
   WC_log = new G4LogicalVolume(WCBox, material_SIO_2, "S1", 0,0,0);
   WC_log->SetVisAttributes(color_SIO_2);

   x_pos=Wwin/2.+Swid/2.;
   G4VPhysicalVolume* WCelem = 
   new G4PVPlacement(0,G4ThreeVector(x_pos,0.,0.0),WC_log,"WCS1a",
				WCBox_log,false,-1);
  WCelem = new G4PVPlacement(0,G4ThreeVector(-x_pos,0.,0.0),WC_log,"WCS1b",
				WCBox_log,false,-1);

   x_pos=Wwin+2*Swid;
   WCBox = new G4Box("WC_S2", x_pos/2., thk/2., Swid/2.);
   WC_log = new G4LogicalVolume(WCBox, material_SIO_2, "S2", 0,0,0);
   WC_log->SetVisAttributes(color_SIO_2);
   z_pos=Lwin/2.+Swid/2.;
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,0.0,z_pos),WC_log,"WCS2a",
				WCBox_log,false,-1);
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,0.0,-z_pos),WC_log,"WCS2b",
				WCBox_log,false,-1);

//----------------------------------------
   G4Box*
   WCFoil_box = new G4Box("WCFoil_box", Wwin/2., foil_thk, Lwin/2.);

   G4Box* 
   WCMylar_box = new G4Box("WCMylar_box", Wwin/2., mylar_thk, Lwin/2.);

   G4LogicalVolume*
   WCFoil = new G4LogicalVolume(WCFoil_box, material_MYLAR, "WCfoil",0,0,0);
   WCFoil->SetVisAttributes(color_MYLAR);
   G4LogicalVolume*
   WCMylar = new G4LogicalVolume(WCMylar_box, material_MYLAR, "WCmylar",0,0,0);
   WCMylar->SetVisAttributes(color_MYLAR);

   y_pos=Gap*4;
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCMylar,"WCM1a",
				WCBox_log,false,-1);
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,-y_pos,0.0),WCMylar,"WCM1b",
				WCBox_log,false,-1);
   y_pos=Gap*1;
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCFoil,"WCF1a",
				WCBox_log,false,-1);
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,-y_pos,0.0),WCFoil,"WCF1b",
				WCBox_log,false,-1);
   y_pos=Gap*3;
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCFoil,"WCF2a",
				WCBox_log,false,-1);
   WCelem = new G4PVPlacement(0,G4ThreeVector(0.0,-y_pos,0.0),WCFoil,"WCF2b",
				WCBox_log,false,-1);
//------------------------------------------------------------------------------
// for sensitive detector -- wire planes
   x_pos=Wwin;//-4.*mm; 
   y_pos=2.*Gap*0.9; 
   z_pos=Lwin;//-4.*mm;
   G4Box*
   WCplane_box = new G4Box("WCplane_box", x_pos/2., y_pos/2., z_pos/2.);
   G4Box*
   WCcell_box = new G4Box("WCcell_box", x_pos/2., y_pos/2., 2.*cm/2.);

   G4LogicalVolume*
   WCplane_log = new G4LogicalVolume(WCplane_box, material_ARCO_2, "WCplane",0,0,0);
   WCplane_log->SetVisAttributes(G4VisAttributes::Invisible);

   G4LogicalVolume*
   WCcell_log = new G4LogicalVolume(WCcell_box, material_ARCO_2, "WCcell",0,0,0);
//   WCcell_log->SetVisAttributes(Gas_VisAtt);
   WCcell_log->SetVisAttributes(color_STEEL);
   
   G4int nr=Lwin/(2.*cm)+0.1;

   new G4PVReplica("WCcells",WCcell_log,WCplane_log, kZAxis, nr, 2.*cm);

   y_pos=-2.*Gap;
   //WCelem = 
   new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCplane_log,"WCpl1",
				WCBox_log,false,ind);
   y_pos=0.;
   //WCelem = 
   new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCplane_log,"WCpl2",
				WCBox_log,false,ind);

   y_pos=2.*Gap;
   //WCelem = 
   new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCplane_log,"WCpl3",
				WCBox_log,false,ind);
   
				
   forSD = WCcell_log;
   return WCBox_log;				

}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......




