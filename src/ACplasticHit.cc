//
/// \file ACplasticHit.cc
/// \brief Implementation of the ACplasticHit class

#include "ACplasticHit.hh"

#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4AttDefStore.hh"
#include "G4AttDef.hh"
#include "G4AttValue.hh"
#include "G4UIcommand.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"


G4ThreadLocal G4Allocator<ACplasticHit>* ACplasticHitAllocator=0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
ACplasticHit::ACplasticHit(G4int id, G4double time, G4double de)
: G4VHit(), 
  fId(id), fTime(time), fde(de)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ACplasticHit::~ACplasticHit()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ACplasticHit::ACplasticHit(const ACplasticHit &right)
: G4VHit(),
  fId(right.fId),
  fTime(right.fTime),
  fde(right.fde)
 {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const ACplasticHit& ACplasticHit::operator=(const ACplasticHit &right)
{
  fId = right.fId;
  fTime = right.fTime;
  fde = right.fde;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int ACplasticHit::operator==(const ACplasticHit &/*right*/) const
{
  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ACplasticHit::Print()
{
  G4cout << "  ACplastic[" << fId << "] " << fTime/ns << " (nsec)" << G4endl;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
