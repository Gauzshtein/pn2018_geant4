/// \file EventAction.cc
/// \brief Implementation of the EventAction class

#include "EventAction.hh"
#include "RunAction.hh"

#include "g4root.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"
#include "G4LorentzVector.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"

#include "iomanip"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction(/*RunAction* runAction*/)
: G4UserEventAction(), ScHCEID_PP(-1), ScHCEID_AC(-1), ScHCEID_Strip(-1), ScHCEID_ElectronStrip(-1), ScHCEID_ElectronPolystirol(-1)
{} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ProtonPlasticHitsCollection* EventAction::GetHitsCollectionPP(G4int hcID, const G4Event* event) const {

    ProtonPlasticHitsCollection* HitsCollection = dynamic_cast<ProtonPlasticHitsCollection*>(
                                                            event->GetHCofThisEvent()->GetHC(hcID)
    );
    if (!HitsCollection) {/*TEST*/
        
    };
            return HitsCollection;
}

ACplasticHitsCollection* EventAction::GetHitsCollectionAC(G4int hcID, const G4Event* event) const {

    ACplasticHitsCollection* HitsCollection = dynamic_cast<ACplasticHitsCollection*>(
                                                            event->GetHCofThisEvent()->GetHC(hcID)
    );
    if (!HitsCollection) {/*TEST*/
        
    };
            return HitsCollection;
}


StripPlasticHitsCollection* EventAction::GetHitsCollectionStrip(G4int hcID, const G4Event* event) const {

    StripPlasticHitsCollection* HitsCollection = dynamic_cast<StripPlasticHitsCollection*>(
                                                            event->GetHCofThisEvent()->GetHC(hcID)
    );
    if (!HitsCollection) {/*TEST*/
        
    };
            return HitsCollection;
}

ElectronStripPlasticHitsCollection* EventAction::GetHitsCollectionElectronStrip(G4int hcID, const G4Event* event) const {

    ElectronStripPlasticHitsCollection* HitsCollection = dynamic_cast<ElectronStripPlasticHitsCollection*>(
                                                            event->GetHCofThisEvent()->GetHC(hcID)
    );
    if (!HitsCollection) {/*TEST*/
        
    };
            return HitsCollection;
}

ElectronPolystirolHitsCollection* EventAction::GetHitsCollectionElectronPolystirol(G4int hcID, const G4Event* event) const {

    ElectronPolystirolHitsCollection* HitsCollection = dynamic_cast<ElectronPolystirolHitsCollection*>(
                                                            event->GetHCofThisEvent()->GetHC(hcID)
    );
    if (!HitsCollection) {/*TEST*/
        
    };
            return HitsCollection;
}

void EventAction::BeginOfEventAction(const G4Event*)
{    
  //fEdep = 0.;
 // G4cout<<G4endl<< "***Begin of Event****"<<G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::EndOfEventAction(const G4Event* event)
{   //BEGIN for end of event
    
 //   G4cout<<G4endl;

    G4double tpp[6], depp[6]; //for proton plastic
    G4double tac[4], deac[4]; //for AC plastic
    G4double destrip[44]; //for strip plastic
    G4double de_electron_strip[9]; //for electron strip plastic
    G4double de_electron_polystirol[1]; //for electron polystirol
    
    for (G4int i = 0; i < 6; i++) {tpp[i]=0.0; depp[i]=0.0;}
    for (G4int i = 0; i < 4; i++) {tac[i]=0.0; deac[i]=0.0;}
    for (G4int i = 0; i < 44; i++) {destrip[i]=0.0;}
    for (G4int i = 0; i < 9; i++) {de_electron_strip[i]=0.0;}
    for (G4int i = 0; i < 1; i++) {de_electron_polystirol[i]=0.0;}
    
    if (ScHCEID_PP==-1) {
        ScHCEID_PP = G4SDManager::GetSDMpointer()->GetCollectionID("ProtonPlasticCollection"); }
        
    if (ScHCEID_AC==-1) {
        ScHCEID_AC = G4SDManager::GetSDMpointer()->GetCollectionID("ACplasticCollection"); }

    if (ScHCEID_Strip==-1) {
        ScHCEID_Strip = G4SDManager::GetSDMpointer()->GetCollectionID("StripPlasticCollection"); }
        
    if (ScHCEID_ElectronStrip==-1) {
        ScHCEID_ElectronStrip = G4SDManager::GetSDMpointer()->GetCollectionID("ElectronStripPlasticCollection"); }    
        
    if (ScHCEID_ElectronPolystirol==-1) {
        ScHCEID_ElectronPolystirol = G4SDManager::GetSDMpointer()->GetCollectionID("ElectronPolystirolCollection"); }       
        
    ProtonPlasticHitsCollection* ScHC_PP = GetHitsCollectionPP(ScHCEID_PP, event);
    for ( G4int i = 0; i < ScHC_PP->entries(); ++i) {
        tpp[(*ScHC_PP)[i]->GetID()]=(*ScHC_PP)[i]->GetTime()/ns;
        depp[(*ScHC_PP)[i]->GetID()]=(*ScHC_PP)[i]->GetEnergy()/MeV;
    }   
     
    ACplasticHitsCollection* ScHC_AC = GetHitsCollectionAC(ScHCEID_AC, event);
    for ( G4int i = 0; i < ScHC_AC->entries(); ++i) {
        tac[(*ScHC_AC)[i]->GetID()]=(*ScHC_AC)[i]->GetTime()/ns;
        deac[(*ScHC_AC)[i]->GetID()]=(*ScHC_AC)[i]->GetEnergy()/MeV;
    }
    
    StripPlasticHitsCollection* ScHC_Strip = GetHitsCollectionStrip(ScHCEID_Strip, event);
    for ( G4int i = 0; i < ScHC_Strip->entries(); ++i) {
        destrip[(*ScHC_Strip)[i]->GetID()]=(*ScHC_Strip)[i]->GetEnergy()/MeV;
    } 
    
    ElectronStripPlasticHitsCollection* ScHC_ElectronStrip = GetHitsCollectionElectronStrip(ScHCEID_ElectronStrip, event);
    for ( G4int i = 0; i < ScHC_ElectronStrip->entries(); ++i) {
        //tpp[(*ScHC_PP)[i]->GetID()]=(*ScHC_PP)[i]->GetTime()/ns;
        de_electron_strip[(*ScHC_ElectronStrip)[i]->GetID()]=(*ScHC_ElectronStrip)[i]->GetEnergy()/MeV;
    } 
    
    ElectronPolystirolHitsCollection* ScHC_ElectronPolystirol = GetHitsCollectionElectronPolystirol(ScHCEID_ElectronPolystirol, event);
    for ( G4int i = 0; i < ScHC_ElectronPolystirol->entries(); ++i) {
        //tpp[(*ScHC_PP)[i]->GetID()]=(*ScHC_PP)[i]->GetTime()/ns;
        de_electron_polystirol[(*ScHC_ElectronPolystirol)[i]->GetID()]=(*ScHC_ElectronPolystirol)[i]->GetEnergy()/MeV;
    }
    //auto analysisManager = G4AnalysisManager::Instance();    
    
    G4int numb_of_VarGenerator=12; // Number of variable of PrimeryGenerator
    G4int numb_of_PP=6; //Number of proton Plastic
    G4int numb_of_AC=4; //Number of AC Plastic
    G4int numb_of_SiPM=44; //Number of SiPM for Strip Plastic
    G4int numb_of_electron_strip=9; //Number of electron Strip Plastic
    G4int numb_of_electron_polystirol=1; //Number of electron Polystirol
    
    G4double depp_sum=0.0, destrip_sumX=0.0, destrip_sumZ=0.0, de_electron_strip_sum=0.0;
    
    for (G4int i = 0; i<numb_of_PP; i++) depp_sum+=depp[i];
    for (G4int i = 0; i<numb_of_SiPM/2; i++) destrip_sumX+=destrip[i];
    for (G4int i = numb_of_SiPM/2; i<numb_of_SiPM; i++) destrip_sumZ+=destrip[i];
    for (G4int i = 0; i<numb_of_electron_strip; i++) de_electron_strip_sum+=de_electron_strip[i];
    //G4cout<<"destrip_X="<<destrip_sumX<<"\t"<<"destrip_Z="<<destrip_sumZ<<G4endl;
    
    //for (G4int i = 0; i<numb_of_SiPM; i++) {if (destrip[i]>0.0) G4cout<<"destrip("<<i<<")= "<<destrip[i]<<"\t"; } G4cout<<G4endl;
    
    
    if (depp_sum>0.0&&destrip_sumX>0.0&&destrip_sumZ>0.0) // if for write ntuple
    { //1   
    
        //for (G4int i = 0; i<numb_of_SiPM; i++) {if (destrip[i]>0.0) G4cout<<"destrip("<<i<<")= "<<destrip[i]<<"\t"; } G4cout<<G4endl;
    
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();    
    //G4cout<<"Summ for PP="<<depp_sum<<G4endl;
    
    
    for (G4int i = 0; i<numb_of_PP; i++) { //energy for PP-plastic
        analysisManager->FillNtupleDColumn(1,numb_of_VarGenerator+i, depp[i]); }
        
    for (G4int i = 0; i<numb_of_PP; i++) { //time for PP-plastic
        analysisManager->FillNtupleDColumn(1,numb_of_VarGenerator+i+numb_of_PP, tpp[i]); }
   
    for (G4int i = 0; i<numb_of_AC; i++) { //energy for AC-plastic
        analysisManager->FillNtupleDColumn(1,numb_of_VarGenerator+i+2*numb_of_PP, deac[i]); } //G4cout<<G4endl;
        
    for (G4int i = 0; i<numb_of_AC; i++) { //time for AC-plastic
        analysisManager->FillNtupleDColumn(1,numb_of_VarGenerator+i+2*numb_of_PP+numb_of_AC, tac[i]); }
    
    for (G4int i = 0; i<numb_of_SiPM; i++) { //energy for Strip Plastic
        analysisManager->FillNtupleDColumn(1,numb_of_VarGenerator+i+2*numb_of_PP+2*numb_of_AC, destrip[i]);
    //        analysisManager->FillNtupleDColumn(i, destrip[i]);
    }

    for (G4int i = 0; i<numb_of_electron_strip; i++) { //energy for Electron Strip Plastic
        analysisManager->FillNtupleDColumn(1,numb_of_VarGenerator+i+2*numb_of_PP+2*numb_of_AC+numb_of_SiPM, de_electron_strip[i]);
      //  G4cout<<"de["<<i<<"]="<<de_electron_strip[i]<<"\t";
    }
    
    for (G4int i = 0; i<numb_of_electron_polystirol; i++) { //energy for Electron Polystirol
        analysisManager->FillNtupleDColumn(1,numb_of_VarGenerator+i+2*numb_of_PP+2*numb_of_AC+numb_of_SiPM+numb_of_electron_strip, de_electron_polystirol[i]);
    }
    //G4cout<<G4endl<<G4endl;
    //analysisManager->FillNtupleDColumn(23, destrip[0]);
    //analysisManager->FillNtupleDColumn(24, destrip[1]);
    
    analysisManager->AddNtupleRow(1);  
    
    }//1 
    
    
//          analysisManager->AddNtupleRow();  
}//END for end of event

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
