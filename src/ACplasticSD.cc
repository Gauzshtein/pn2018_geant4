//
/// \file ACplasticSD.cc
/// \brief Implementation of the ACplasticSD class

#include "ACplasticSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ACplasticSD::ACplasticSD(G4String name) : 
G4VSensitiveDetector(name),
fHitsCollection(nullptr), fHCID(-1)
                      { collectionName.insert("ACplasticCollection"); }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ACplasticSD::~ACplasticSD() 
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ACplasticSD::Initialize(G4HCofThisEvent* hce)
{
  // Create hits collection for proton plastic
  fHitsCollection = new ACplasticHitsCollection(SensitiveDetectorName, collectionName[0]); 
  // Add this collection in hce
  if (fHCID<0) { 
    fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(fHitsCollection); 
  }
  hce->AddHitsCollection(fHCID,fHitsCollection);
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool ACplasticSD::ProcessHits(G4Step* aStep, 
                                     G4TouchableHistory*)
{  
  G4double edep = aStep->GetTotalEnergyDeposit(); //Get energy deposit
  if (edep==0.) return true;
  auto preStepPoint = aStep->GetPreStepPoint();
  G4int copyNo = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(1); //Get number of AC plastic
//  if (copyNo==1) G4cout<<G4endl<<" AC number is "<<copyNo<<G4endl;
  auto hitTime = preStepPoint->GetGlobalTime(); //Get time for proton plastic

  // check if this finger already has a hit
  G4int ix = -1;
  for (G4int i=0;i<fHitsCollection->entries();i++) {
    if ((*fHitsCollection)[i]->GetID()==copyNo) {
      ix = i;
      break;
    }
  }

  if (ix>=0) {
    if ((*fHitsCollection)[ix]->GetTime()>hitTime) { (*fHitsCollection)[ix]->SetTime(hitTime);}
    (*fHitsCollection)[ix]->AddEnergy(edep); }
  else {
    // if not, create a new hit and set it to the collection
    ACplasticHit* hit = new ACplasticHit(copyNo,hitTime,edep);
    fHitsCollection->insert(hit);
  }
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
void B1PlasticSD::EndOfEvent(G4HCofThisEvent*)
{
  if ( verboseLevel>1 ) { 
     G4int nofHits = fHitsCollection->entries();
     G4cout << G4endl
            << "-------->Hits Collection: in this event they are " << nofHits 
            << " hits in the tracker chambers: " << G4endl;
     for ( G4int i=0; i<nofHits; i++ ) (*fHitsCollection)[i]->Print();
  }
}
*/
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
