#ifndef H_CONFIG_PN_PROJECT_H
#define H_CONFIG_PN_PROJECT_H 1

#include <string>

class ConfigPN
{
protected :
    static ConfigPN* _self;
    ConfigPN();
    virtual ~ConfigPN();

    std::string _rootFileName;
    std::string _treeName;
    
public :
    static ConfigPN* Instance();

    void set_root_file_name( std::string root_file_name_ ) { _rootFileName = root_file_name_; }
    void set_tree_name( std::string tree_file_name_) { _treeName = tree_file_name_; }

    std::string get_root_file_name() { return _rootFileName; }
    std::string get_tree_name() { return _treeName; }
};

#endif  // H_CONFIG_PN_PROJECT_H
