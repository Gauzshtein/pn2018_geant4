//
/// \file ElectronPolystirolHit.hh
/// \brief Definition of the ElectronPolystirolHit class

#ifndef ElectronPolystirolHit_h
#define ElectronPolystirolHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

class G4AttDef;
class G4AttValue;

/// Plastic hit
///
/// It records:
/// - the strip ID
/// - the particle time


class ElectronPolystirolHit : public G4VHit
{
  public:
    ElectronPolystirolHit(G4int i,G4double t,G4double e);
    ElectronPolystirolHit(const ElectronPolystirolHit &right);

    virtual ~ElectronPolystirolHit();

    const ElectronPolystirolHit& operator=(const ElectronPolystirolHit &right);
    int operator==(const ElectronPolystirolHit &right) const;
    
    inline void *operator new(size_t);
    inline void operator delete(void*aHit);
    
//    void Print();
    
    
    G4int GetID() const { return fId; }

    void SetTime(G4double val) { fTime = val; }

    G4double GetTime() const { return fTime; }
    
    void AddEnergy(G4double e) {fde += e;}

    G4double GetEnergy() const { return fde; }
      
  private:
    G4int fId;
    G4double fTime;
    G4double fde;
    };

using ElectronPolystirolHitsCollection = G4THitsCollection<ElectronPolystirolHit>;

extern G4ThreadLocal G4Allocator<ElectronPolystirolHit>* ElectronPolystirolHitAllocator;

inline void* ElectronPolystirolHit::operator new(size_t)
{
  if (!ElectronPolystirolHitAllocator) {
       ElectronPolystirolHitAllocator = new G4Allocator<ElectronPolystirolHit>;
  }
  return (void*)ElectronPolystirolHitAllocator->MallocSingle();
}

inline void ElectronPolystirolHit::operator delete(void*aHit)
{
  ElectronPolystirolHitAllocator->FreeSingle((ElectronPolystirolHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
