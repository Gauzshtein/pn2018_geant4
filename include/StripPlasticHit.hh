//
/// \file StripPlasticHit.hh
/// \brief Definition of the StripPlasticHit class

#ifndef StripPlasticHit_h
#define StripPlasticHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

class G4AttDef;
class G4AttValue;

/// StripPlastic hit
///
/// It records:
/// - the strip ID
/// - the particle time


class StripPlasticHit : public G4VHit
{
  public:
    StripPlasticHit(G4int i,G4double t,G4double e);
    StripPlasticHit(const StripPlasticHit &right);

    virtual ~StripPlasticHit();

    const StripPlasticHit& operator=(const StripPlasticHit &right);
    int operator==(const StripPlasticHit &right) const;
    
    inline void *operator new(size_t);
    inline void operator delete(void*aHit);
    
    void Print();
    
    
    G4int GetID() const { return fId; }

    void SetTime(G4double val) { fTime = val; }

    G4double GetTime() const { return fTime; }
    
    void AddEnergy(G4double e) {fde += e;}

    G4double GetEnergy() const { return fde; }
      
  private:
    G4int fId;
    G4double fTime;
    G4double fde;
    };

using StripPlasticHitsCollection = G4THitsCollection<StripPlasticHit>;

extern G4ThreadLocal G4Allocator<StripPlasticHit>* StripPlasticHitAllocator;

inline void* StripPlasticHit::operator new(size_t)
{
  if (!StripPlasticHitAllocator) {
       StripPlasticHitAllocator = new G4Allocator<StripPlasticHit>;
  }
  return (void*)StripPlasticHitAllocator->MallocSingle();
}

inline void StripPlasticHit::operator delete(void*aHit)
{
  StripPlasticHitAllocator->FreeSingle((StripPlasticHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
