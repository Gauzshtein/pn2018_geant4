//
/// \file ACplasticHit.hh
/// \brief Definition of the ACplasticHit class

#ifndef ACplasticHit_h
#define ACplasticHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

class G4AttDef;
class G4AttValue;

/// Plastic hit
///
/// It records:
/// - the strip ID
/// - the particle time


class ACplasticHit : public G4VHit
{
  public:
    ACplasticHit(G4int i,G4double t,G4double e);
    ACplasticHit(const ACplasticHit &right);

    virtual ~ACplasticHit();

    const ACplasticHit& operator=(const ACplasticHit &right);
    int operator==(const ACplasticHit &right) const;
    
    inline void *operator new(size_t);
    inline void operator delete(void*aHit);
    
    void Print();
    
    
    G4int GetID() const { return fId; }

    void SetTime(G4double val) { fTime = val; }

    G4double GetTime() const { return fTime; }
    
    void AddEnergy(G4double e) {fde += e;}

    G4double GetEnergy() const { return fde; }
      
  private:
    G4int fId;
    G4double fTime;
    G4double fde;
    };

using ACplasticHitsCollection = G4THitsCollection<ACplasticHit>;

extern G4ThreadLocal G4Allocator<ACplasticHit>* ACplasticHitAllocator;

inline void* ACplasticHit::operator new(size_t)
{
  if (!ACplasticHitAllocator) {
       ACplasticHitAllocator = new G4Allocator<ACplasticHit>;
  }
  return (void*)ACplasticHitAllocator->MallocSingle();
}

inline void ACplasticHit::operator delete(void*aHit)
{
  ACplasticHitAllocator->FreeSingle((ACplasticHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
