//
/// \file ElectronStripPlasticSD.hh
/// \brief Definition of the ElectronStripPlasticSD class

#ifndef ElectronStripPlasticSD_h
#define ElectronStripPlasticSD_h 1

#include "G4VSensitiveDetector.hh"

#include "ElectronStripPlasticHit.hh"

#include <vector>

class G4Step;
class G4HCofThisEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/// ElectronStripPlastic sensitive detector class
///
/// The hits are accounted in hits in ProcessHits() function which is called
/// by Geant4 kernel at each step. A hit is created with each step with non zero 
/// energy deposit.

class ElectronStripPlasticSD : public G4VSensitiveDetector
{
  public:
    ElectronStripPlasticSD(G4String name);
    virtual ~ElectronStripPlasticSD();
  
    // methods from base class
    virtual void   Initialize(G4HCofThisEvent* HCE);
    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);
//    virtual void   EndOfEvent(G4HCofThisEvent* hitCollection);
    
  private:
    ElectronStripPlasticHitsCollection* fHitsCollection; 
    G4int fHCID;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
