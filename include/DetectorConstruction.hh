//
/// \file DetectorConstruction.hh
/// \brief Definition of the B1DetectorConstruction class

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include <vector>
#include "G4RotationMatrix.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4ThreeVector.hh"
#include "G4NistManager.hh"

#include "tls.hh"


class G4VPhysicalVolume;
class G4LogicalVolume;
class G4VisAttributes;
//class PlasticSD;
/// Detector construction class to define materials and geometry.

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    G4LogicalVolume *PL_log, *PLBox_log, *AC_log, *ACBox_log, *scint_log, *PLSTR_log, *Strip_e_log;
    DetectorConstruction();
    virtual ~DetectorConstruction();

    virtual G4VPhysicalVolume* Construct();
    virtual void ConstructSDandField();
    
    G4LogicalVolume* GetScoringVolume() const { return fScoringVolume; }

  protected:
    G4LogicalVolume*  fScoringVolume;
    
    private:
      std::vector<G4VisAttributes*> fVisAttributes;
      
      G4LogicalVolume* ConstructWC(G4double Lwin, G4double Wwin, G4int i, G4LogicalVolume*& forSD); 

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
