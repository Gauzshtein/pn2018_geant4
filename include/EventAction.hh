/// \file EventAction.hh
/// \brief Definition of the EventAction class

#ifndef EventAction_h
#define EventAction_h 1

#include "ProtonPlasticSD.hh"
#include "ACplasticSD.hh"
#include "StripPlasticSD.hh"
#include "ElectronStripPlasticSD.hh"
#include "ElectronPolystirolSD.hh"

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4SDManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//class EventAction;

/// Event action class
///

class EventAction : public G4UserEventAction
{
  public:
    EventAction(/*RunAction* runAction*/);
    virtual ~EventAction();

    virtual void BeginOfEventAction(const G4Event* event);
    virtual void EndOfEventAction(const G4Event* event);

   private:
      ProtonPlasticHitsCollection* GetHitsCollectionPP(G4int hcID, const G4Event* event) const;
      ACplasticHitsCollection* GetHitsCollectionAC(G4int hcID, const G4Event* event) const;
      StripPlasticHitsCollection* GetHitsCollectionStrip(G4int hcID, const G4Event* event) const;
      ElectronStripPlasticHitsCollection* GetHitsCollectionElectronStrip(G4int hcID, const G4Event* event) const;
      ElectronPolystirolHitsCollection* GetHitsCollectionElectronPolystirol(G4int hcID, const G4Event* event) const;
      G4int ScHCEID_PP, ScHCEID_AC, ScHCEID_Strip, ScHCEID_ElectronStrip, ScHCEID_ElectronPolystirol;
   
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
