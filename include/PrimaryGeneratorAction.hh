/// \file PrimaryGeneratorAction.hh
/// \brief Definition of the PrimaryGeneratorAction class

#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "globals.hh"

#include "TTree.h"
#include "TFile.h"

class G4ParticleGun;
class G4Event;
class G4Box;

/// The primary generator action class with particle gun.
///

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    PrimaryGeneratorAction();    
    virtual ~PrimaryGeneratorAction();

    // method from the base class
    virtual void GeneratePrimaries(G4Event*);  
     // method to access particle gun
    const G4ParticleGun* GetParticleGun() const { return fParticleGun; }
  
  private:
    G4ParticleGun*  fParticleGun; // pointer a to G4 gun class
    G4Box* fEnvelopeBox;
    TFile *evfile;
    TTree *T;
    G4int i_entry;
    G4int nentries;
    G4float efot;
    G4int nreac;
    G4float vx;
    G4float vy;
    G4float vz;
    G4int np;
    G4int idg[11];   //[np]
    G4int qch[11];   //[np]
    G4float mass[11];   //[np]
    G4float imp[11];   //[np]
    G4float cx[11];   //[np]
    G4float cy[11];   //[np]
    G4float cz[11];
    };

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
