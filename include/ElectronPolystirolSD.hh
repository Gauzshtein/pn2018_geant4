//
/// \file ElectronPolystirolSD.hh
/// \brief Definition of the ElectronPolystirolSD class

#ifndef ElectronPolystirolSD_h
#define ElectronPolystirolSD_h 1

#include "G4VSensitiveDetector.hh"

#include "ElectronPolystirolHit.hh"

#include <vector>

class G4Step;
class G4HCofThisEvent;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/// ElectronPolystirol sensitive detector class
///
/// The hits are accounted in hits in ProcessHits() function which is called
/// by Geant4 kernel at each step. A hit is created with each step with non zero 
/// energy deposit.

class ElectronPolystirolSD : public G4VSensitiveDetector
{
  public:
    ElectronPolystirolSD(G4String name);
    virtual ~ElectronPolystirolSD();
  
    // methods from base class
    virtual void   Initialize(G4HCofThisEvent* HCE);
    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);
//    virtual void   EndOfEvent(G4HCofThisEvent* hitCollection);
    
  private:
    ElectronPolystirolHitsCollection* fHitsCollection; 
    G4int fHCID;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
