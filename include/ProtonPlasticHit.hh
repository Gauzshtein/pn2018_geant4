//
/// \file ProtonPlasticHit.hh
/// \brief Definition of the ProtonPlasticHit class

#ifndef ProtonPlasticHit_h
#define ProtonPlasticHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

class G4AttDef;
class G4AttValue;

/// Plastic hit
///
/// It records:
/// - the strip ID
/// - the particle time


class ProtonPlasticHit : public G4VHit
{
  public:
    ProtonPlasticHit(G4int i,G4double t,G4double e);
    ProtonPlasticHit(const ProtonPlasticHit &right);

    virtual ~ProtonPlasticHit();

    const ProtonPlasticHit& operator=(const ProtonPlasticHit &right);
    int operator==(const ProtonPlasticHit &right) const;
    
    inline void *operator new(size_t);
    inline void operator delete(void*aHit);
    
    void Print();
    
    
    G4int GetID() const { return fId; }

    void SetTime(G4double val) { fTime = val; }

    G4double GetTime() const { return fTime; }
    
    void AddEnergy(G4double e) {fde += e;}

    G4double GetEnergy() const { return fde; }
      
  private:
    G4int fId;
    G4double fTime;
    G4double fde;
    };

using ProtonPlasticHitsCollection = G4THitsCollection<ProtonPlasticHit>;

extern G4ThreadLocal G4Allocator<ProtonPlasticHit>* ProtonPlasticHitAllocator;

inline void* ProtonPlasticHit::operator new(size_t)
{
  if (!ProtonPlasticHitAllocator) {
       ProtonPlasticHitAllocator = new G4Allocator<ProtonPlasticHit>;
  }
  return (void*)ProtonPlasticHitAllocator->MallocSingle();
}

inline void ProtonPlasticHit::operator delete(void*aHit)
{
  ProtonPlasticHitAllocator->FreeSingle((ProtonPlasticHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
