//
/// \file ElectronStripPlasticHit.hh
/// \brief Definition of the ElectronStripPlasticHit class

#ifndef ElectronStripPlasticHit_h
#define ElectronStripPlasticHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

class G4AttDef;
class G4AttValue;

/// Plastic hit
///
/// It records:
/// - the strip ID
/// - the particle time


class ElectronStripPlasticHit : public G4VHit
{
  public:
    ElectronStripPlasticHit(G4int i,G4double t,G4double e);
    ElectronStripPlasticHit(const ElectronStripPlasticHit &right);

    virtual ~ElectronStripPlasticHit();

    const ElectronStripPlasticHit& operator=(const ElectronStripPlasticHit &right);
    int operator==(const ElectronStripPlasticHit &right) const;
    
    inline void *operator new(size_t);
    inline void operator delete(void*aHit);
    
//    void Print();
    
    
    G4int GetID() const { return fId; }

    void SetTime(G4double val) { fTime = val; }

    G4double GetTime() const { return fTime; }
    
    void AddEnergy(G4double e) {fde += e;}

    G4double GetEnergy() const { return fde; }
      
  private:
    G4int fId;
    G4double fTime;
    G4double fde;
    };

using ElectronStripPlasticHitsCollection = G4THitsCollection<ElectronStripPlasticHit>;

extern G4ThreadLocal G4Allocator<ElectronStripPlasticHit>* ElectronStripPlasticHitAllocator;

inline void* ElectronStripPlasticHit::operator new(size_t)
{
  if (!ElectronStripPlasticHitAllocator) {
       ElectronStripPlasticHitAllocator = new G4Allocator<ElectronStripPlasticHit>;
  }
  return (void*)ElectronStripPlasticHitAllocator->MallocSingle();
}

inline void ElectronStripPlasticHit::operator delete(void*aHit)
{
  ElectronStripPlasticHitAllocator->FreeSingle((ElectronStripPlasticHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
